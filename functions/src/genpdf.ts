import { DH_NOT_SUITABLE_GENERATOR } from "constants";
import { runInDebugContext } from "vm";
import { doesNotThrow } from "assert";

const admin = require('firebase-admin');
const https = require('https');
const PDFDocument = require('pdfkit');


export const genrPDF = async function (request, response) {
    let text = request.query.text;
    let formRef;
    let docRef = await admin.firestore().collection("formrefs").doc(text);
    await docRef.get().then(async function (doc) {
        formRef = doc.data();
        text = formRef.form_key;
        docRef = await admin.firestore().collection("forms").doc(text);
    });
    const pdfdoc = new PDFDocument({ autoFirstPage: false });
    pdfdoc.info['Title'] = text + ".pdf";
    pdfdoc.pipe(response);
    await genIt(pdfdoc, text, formRef);
    pdfdoc.end();

};

export const genPDF = async function (request, response) {
    console.log("genPDF()");
    const text = request.query.text;
    const pdfdoc = new PDFDocument({ autoFirstPage: false });
    pdfdoc.info['Title'] = text + ".pdf";
    pdfdoc.pipe(response);
    await genIt(pdfdoc, text, null);
    pdfdoc.end();
};

async function genIt(pdfdoc, formId, formRef) {
    const docRef = admin.firestore().collection("forms").doc(formId);
    await docRef.get().then(async function (doc) {
        if (doc.exists) {
            const pagesRef = admin.firestore().collection("pages").where("formKey", "==", formId).orderBy("pageno");
            await pagesRef.get().then(async function (querySnapshot) {
                let cnt = 1;
                for (var h = 0; h < querySnapshot.docs.length; h++) {
                    const page = querySnapshot.docs[h];
                    await doPage(page, pdfdoc, formRef);
                    cnt++;
                }
            }).catch(function (error) {
                // console.log("Error getting documents: ", error);
            })
        } else {
            // console.log("No such document!");
        }
    }).catch(function (error) {
        // console.log("Error gettting documents: ", error);
    });
}

async function doPage(page, pdfdoc, formRef) {
    console.log("doPage() start")
    pdfdoc.addPage({ margin: 0 });
    let x = 5;
    let y = 5;
    const multiplier_x = .70;
    const multiplier_y = .70;
    pdfdoc.moveTo(x, y)
        .lineTo(607, 5)
        .lineTo(607, 787)
        .lineTo(5, 787)
        .lineTo(x, y)
        .stroke();
    for (let i = 0; i < page.data().tables.length; i++) {
        const table = page.data().tables[i];
        y = await doTable(table, pdfdoc, x, y, multiplier_x, multiplier_y, formRef);
    }
    console.log("doPage() end")

}

async function doTable(table, pdfdoc, x, y, multiplier_x, multiplier_y, formRef) {
    // console.log("doTable() start");
    let y_ = y + 2; // margin
    let x_ = x;

    let tableWidth = 5;
    // console.log("table.cols.length = " + table.cols.length);
    for (let p = 0; p < table.cols.length; p++) {
        tableWidth = tableWidth + (table.cols[p] * multiplier_x);
    }
    // console.log("tableWidth = " + tableWidth);
    const offset = (597 - tableWidth) / 2;
    // console.log("offset = " + offset);
    for (let row = 0; row < table.rows.length; row++) {
        const _row = table.rows[row];
        const rowheight = _row.height * multiplier_y;
        x_ = 10 + offset;
        for (let col = 0; col < _row.cells.length; col++) {
            // console.log("doCell2");
            const cell = _row.cells[col];
            await doCell(table, cell, row, col, pdfdoc, x_, y_, multiplier_x, multiplier_y, formRef);
            x_ = x_ + (table.cols[col] * multiplier_x);
        }
        y_ = y_ + (rowheight);
    }
    // console.log("doTable() end");
    return y_;
}

async function doCell(table, cell, row, col, pdfdoc, x, y, multiplier_x, multiplier_y, formRef) {
    console.log("doCell() start");

    // determine if cell is visible or hidden and skip if hidden.
    const colwidth = table.cols[col] * multiplier_x;
    if (cell.display) {
        // console.log("doCell() end - hidden");
        return;
    }

    // calculate cell width and height
    let cellwidth = 0;
    let cellheight = 0;
    const colspan = cell.colspan;
    const rowspan = cell.rowspan;
    for (let i = 0; i < colspan; i++) {
        cellwidth = cellwidth + (table.cols[col + i] * multiplier_x);
    }
    for (let i = 0; i < rowspan; i++) {
        cellheight = cellheight + (table.rows[row + i].height * multiplier_y);
    }

    // calculate the width and height of the cell contents.
    // console.log("calculate width and height");
    let ewidth: number = 0;
    let eheight: number = 0;

    for (let l = 0; l < cell.cellels.length; l++) {
        let cellel = cell.cellels[l];
        if (cellel.type == "Input") {
            // console.log("doInput() start");
            ewidth = ewidth + (cellel.width * multiplier_x);
            if (cellel.height * multiplier_y > eheight) {
                eheight = (cellel.height * multiplier_y);
            }
        }
        if (cellel.type == "Text") {
            pdfdoc.fontSize((cellel.ftsize) * .7);
            ewidth = ewidth + pdfdoc.widthOfString(cellel.text);
            if ((pdfdoc.heightOfString(cellel.text, {width: 1000}) * multiplier_y) > eheight) {
                eheight = pdfdoc.heightOfString(cellel.text, {width: 1000}) * multiplier_y;
            }
        }
        if (cellel.type == "Image") {
            ewidth = ewidth + (cellel.width * multiplier_x);
            if (cellel.height * multiplier_y > eheight) {
                eheight = (cellel.height * multiplier_y);
            }
        }
        if (cellel.type == "HR") {
            ewidth = ewidth + (cellel.width * multiplier_x);
            if (cellel.height * multiplier_y > eheight) {
                eheight = (cellel.height * multiplier_y);
            }
        }
        if (cellel.type == "Table") {
            for (let a = 0; a < cellel.table.rows.length; a++) {
                // console.log("rows length = " + cell.cellels[l].table.rows.length);
                eheight = eheight + (cellel.table.rows[a].height * multiplier_y);
                // console.log("eheight = " + eheight);
            }
            for (let a = 0; a < cellel.table.cols.length; a++) {
                // console.log("cols length = " + cell.cellels[l].table.cols.length);
                ewidth = ewidth + (cellel.table.cols[a] * multiplier_x);
                // console.log("ewidth = " + ewidth);
            }
        }
    }
    // console.log("final ewidth = " + ewidth);
    // console.log("final eheight = " + eheight);

    // x_, y_ are where the contents of the cell elements should be drawn and is dependant on horizontal and vertical alignment.
    let y_: number = 0;
    let x_: number = 0;

    const topL_x = x;
    const topL_y = y;
    const botR_x = x + (cellwidth);
    const botR_y = y + (cellheight);

    // console.log("topL_x = " + topL_x);
    // console.log("topL_y = " + topL_y);
    // console.log("botR_x = " + botR_x);
    // console.log("botR_y = " + botR_y);
    // console.log("cell_width = " + cellwidth);
    // console.log("cell_height = " + cellheight);

    // Adjust the start point of the elements depending on the horizontal and vertical alignment. 
    // console.log("adjust horizontal and vertical alignment");
    if (cell.halign == "Center") {
        x_ = topL_x + ((cellwidth - ewidth) / 2);
        // console.log("CENTER X_ = " + x_);
    } else if (cell.halign == "Left") {
        x_ = topL_x;
    } else if (cell.halign == "Right") {
        x_ = (botR_x - ewidth);
    }
    if (cell.valign == "Middle") {
        y_ = topL_y + ((cellheight - eheight) / 2);
    } else if (cell.valign == "Top") {
        y_ = topL_y;
    } else if (cell.valign == "Bottom") {
        y_ = (botR_y - eheight);
    }
    if (ewidth > cellwidth) {
        x_ = topL_x;
    }

    // Draw the cell elements.
    // console.log("draw cell elements");
    for (let b = 0; b < cell.cellels.length; b++) {
        const cellel = cell.cellels[b];
        if (cellel.type === "Text") {
            console.log("text = " + cellel.text);
            console.log("height of text = " + pdfdoc.heightOfString(cellel.text));
            console.log("cell_height = " + cellheight);
            console.log("eheight = " + eheight);
            console.log("ftsize = " + cellel.ftsize);
            x_ = await doText(cellel, x_, y_, pdfdoc);
        } else if (cellel.type === "Image") {
            x_ = await doImage(cellel, x_, y_, multiplier_x, multiplier_y, pdfdoc);
        } else if (cellel.type === "Input") {
            x_ = await doInput(cellel, x_, y_, multiplier_x, multiplier_y, pdfdoc);
        } else if (cellel.type === "HR") {
            x_ = await doHr(cellel, x_, y_, multiplier_x, multiplier_y, pdfdoc);
        } else if (cellel.type === "Table") {
            await doInnerTable(cellel.table, ewidth, eheight, x_, y_, multiplier_x, multiplier_y, pdfdoc, formRef);
        }
    }

    // Draw the cell border.
    // console.log("draw cell border");
    pdfdoc.moveTo(x, y)
        .lineTo(x + (cellwidth), y)
        .lineTo(x + (cellwidth), y + (cellheight))
        .lineTo(x, y + (cellheight))
        .lineTo(x, y)
        .stroke();
    console.log("doCell() end");
}

async function doHr(cellel, x_, y_, multiplier_x, multiplier_y, pdfdoc) {
    pdfdoc.lineWidth(cellel.size * multiplier_y);
    pdfdoc.moveTo(x_, y_)
        .lineTo(x_ + (cellel.width * multiplier_x), y_)
        .stroke();
    pdfdoc.lineWidth(1);
    let x1 = x_ + (cellel.width * multiplier_x);
    return x1
}

async function doImage(cellel, x_, y_, multiplier_x, multiplier_y, pdfdoc) {
    // console.log("before to call toBase64()");
    // console.log("cell width " + cell_width);
    // console.log("ewidth " + ewidth);
    try {
        await toBase64(cellel, x_, y_, pdfdoc);
        // pdfdoc.moveTo(x_, y_)
        //     .lineTo(x_ + (cellEl.width * multiplier_x), y_)
        //     .lineTo(x_ + (cellEl.width * multiplier_x), y_ + (cellEl.height * multiplier_x))
        //     .lineTo(x_, y_ + (cellEl.height * multiplier_x))
        //     .lineTo(x_, y_)
        //     .stroke();
    } catch (e) {
        // console.error("Error in toBase64()");
    }
    // console.log("after call to taBase64()");
    let x1 = x_ + cellel.width;
    return x1;
}

async function doText(cellel, x_, y_, pdfdoc) {
    if (!cellel.ftsize) {
        cellel.ftsize = 12;
    }
    pdfdoc.fontSize((cellel.ftsize) * .7);
    pdfdoc.text(cellel.text, x_, (y_ + 2));
    let x1 = x_ + pdfdoc.widthOfString(cellel.text);
    return x1;
}

async function doInput(cellel, x_, y_, multiplier_x, multiplier_y, pdfdoc) {
    let ival = cellel.value;
    pdfdoc.fontSize(11);
    let x1 = x_ + 3;
    pdfdoc.text(ival, x1 + 1, y_ + 3);
    pdfdoc.moveTo(x1, y_)
        .lineTo(x1 + (cellel.width * multiplier_x), y_)
        .lineTo(x1 + (cellel.width * multiplier_x), y_ + (cellel.height * multiplier_y))
        .lineTo(x1, y_ + (cellel.height * multiplier_y))
        .lineTo(x1, y_)
        .stroke();
    x1 = x_ + cellel.width;
    return x1;
}

async function doInnerTable(table, ewidth, eheight, x, y, multiplier_x, multiplier_y, pdfdoc, formRef) {
    let tableWidth = 5;
    // console.log("table.cols.length = " + table.cols.length);
    for (let p = 0; p < table.cols.length; p++) {
        tableWidth = tableWidth + (table.cols[p] * multiplier_x);
    }
    // console.log("tableWidth = " + tableWidth);
    // console.log("offset = " + offset);
    let x_ = x;
    let x1 = x;
    let y1 = y;
    for (let row = 0; row < table.rows.length; row++) {
        // console.log("inner row = " + row);
        const _row = table.rows[row];
        const rowheight = (_row.height * multiplier_y);
        x1 = x_;
        for (let col = 0; col < _row.cells.length; col++) {
            // console.log("doCell2");
            const colwidth = table.cols[col] * multiplier_x;
            const cell = _row.cells[col];
            await doCell(table, cell, row, col, pdfdoc, x1, y1, multiplier_x, multiplier_y, formRef);
            x1 = x1 + (table.cols[col] * multiplier_x);
        }
        y1 = y1 + (rowheight);

    }
    // console.log("leaving Inner Table!");

}

function toBase64(cellEl, x, y, pdfdoc) {
    //    console.log("toBase64(url) #1");
    return new Promise(async (resolve, reject) => {
        //        console.log("toBase64(url) in Promise #2");
        var req = await https.get(cellEl.url, (res) => {
            res.setEncoding('base64');
            let body = "data:" + res.headers["content-type"] + ";base64,";
            // console.log('statusCode:', res.statusCode);
            // console.log('headers:', res.headers);
            res.on('data', (d) => {
                // console.log('on data');
                body += d;
            });
            res.on('end', () => {
                //                console.log('toBase64(url) end body = ' + body);
                pdfdoc.image(body, x, y, { scale: .70 });
                resolve(res);
            });
        });
        req.on('error', err => {
            // console.error('error!');
            reject(err);
        });
    });
}

export const getPDF = async function (formKey, archive) {
    var pdfdoc = new PDFDocument({ autoFirstPage: false });

    await genIt(pdfdoc, formKey, null);
    var buffers = [];
    pdfdoc.on('data', buffers.push.bind(buffers));
    pdfdoc.on('end', function () {
        archive.append(Buffer.concat(buffers), { name: formKey + ".pdf" });
    });
    pdfdoc.end();

};

export const getrefPDF = async function (formrefKey, archive) {
    // const bucket = admin.storage().bucket("cxforms-6dcec.appspot.com");

    let formRef;
    let formKey;
    let docRef = await admin.firestore().collection("formrefs").doc(formrefKey);
    await docRef.get().then(async function (doc) {
        formRef = doc.data();
        // console.log("form_key = " + formRef.form_key);
        formKey = formRef.form_key;
        docRef = await admin.firestore().collection("forms").doc(formKey);
    });

    var pdfdoc = new PDFDocument({ autoFirstPage: false });
    await genIt(pdfdoc, formKey, formRef);
    var buffers = [];
    pdfdoc.on('data', buffers.push.bind(buffers));
    pdfdoc.on('end', function () {
        archive.append(Buffer.concat(buffers), { name: formKey + ".pdf" });
    });
    pdfdoc.end();

};
