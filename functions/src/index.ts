import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as firebase from 'firebase/app';
import * as PDFDocument from 'pdfkit';
import { genPDF, genrPDF } from './genpdf';
import { testingExporting } from './testing';
import { pdfReport } from './pdfreport';
import { pdfrefReport } from './pdfrefreport';
const https = require('https');

admin.initializeApp(functions.config().firebase);

export const helloWorld = functions.https.onRequest((request, response) => {
    response.send("Welcome to the world!");
});

export const generatePDF = functions.https.onRequest(genPDF);
export const pdfRef = functions.https.onRequest(genrPDF);
export const test = functions.https.onRequest(testingExporting);
export const genReport = functions.https.onRequest(pdfReport);
export const genrefReport = functions.https.onRequest(pdfrefReport);

function transferFailed(evt) {
    console.log("An error occured.")
}

function transferComplete(evt) {
    console.log("The transfer is complete.")
}

