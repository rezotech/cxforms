import * as functions from 'firebase-functions';
// import * as admin from 'firebase-admin';
import * as firebase from 'firebase/app';
// import * as PDFDocument from 'pdfkit';
import { toBase64 } from './common';
// import { getPDF } from './genpdf';

const pdf = require('./genpdf');
const https = require('https');
const admin = require('firebase-admin');
const fs = require('fs');
const archiver = require('archiver');
const PDFDocument = require('pdfkit');


export const pdfrefReport = async function (request, response) {
    var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });
    // listen for all archive data to be written
    // 'close' event is fired only when a file descriptor is involved
    response.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    // This event is fired when the data source is drained no matter what was the data source.
    // It is not part of this library but rather from the NodeJS Stream API.
    // @see: https://nodejs.org/api/stream.html#stream_event_end
    response.on('end', function () {
        console.log('Data has been drained');
    });

    // good practice to catch warnings (ie stat failures and other non-blocking errors)
    archive.on('warning', function (err) {
        if (err.code === 'ENOENT') {
            // log warning
        } else {
            // throw error
            throw err;
        }
    });

    // good practice to catch this error explicitly
    archive.on('error', function (err) {
        throw err;
    });
    
    const projectKey = request.query.projectKey;
    const assetrefs = await admin.firestore().collection('assetrefs').where('project_key', '==', projectKey);
    let formrefKeys = [];

    await assetrefs.get().then(async (o) => {
        for (let i = 0; i < o.docs.length; i++) {
            const assetrefKey = o.docs[i].data()['$key'];
            const formrefs = await admin.firestore().collection('formrefs').where('assetref_key', '==', assetrefKey);
            await formrefs.get().then(async (p) => {
                for (let j = 0; j < p.docs.length; j++) {
                    const formrefKey = p.docs[j].data()['$key'];
                    formrefKeys.push(formrefKey);
                    console.log("processing formref = " + formrefKey);
                    await pdf.getrefPDF(formrefKey, archive);
                }
            }).catch(e => {
                console.log(e);
            });
        }
    });
    // pipe archive data to the response
    response.setHeader("Content-Type", "application/octet-stream");
    response.setHeader('Content-Disposition', 'attachment; filename=test.zip');
    archive.pipe(response);
    
    setTimeout(() => {

        archive.finalize();

    },500);
  
};
