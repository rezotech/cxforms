const https = require('https');

export function toBase64(cellEl, x, y, pdfdoc) {
    console.log("toBase64(url) #1");
    return new Promise(async(resolve, reject) => {
        console.log("toBase64(url) in Promise #2");
        const req = await https.get(cellEl.url, (res) => {
            res.setEncoding('base64');
            let body = "data:" + res.headers["content-type"] + ";base64,";
            // console.log('statusCode:', res.statusCode);
            // console.log('headers:', res.headers);
            res.on('data', (d) => {
                // console.log('on data');
                body += d;
            });
            res.on('end', () => {
                console.log('toBase64(url) end body = ' + body);
                pdfdoc.image(body, x, y, {scale: .7});
                resolve(res);
            });
        });
        req.on('error', err => {
            console.error('error!');
            reject(err);
        });
    });
}

export async function toBase642(url) {
    let body = "";
    const getReq = await https.get(url, async (res) => {
        res.setEncoding('base64');
        body = "data:" + res.headers["content-type"] + ";base64,";
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);

        res.on('data', (d) => {
            console.log('on data');
            body += d;
        });
        res.on('end', () => {
            console.log('body = ' + body);
            return body;
        });
    });
    getReq.end();
    getReq.on('error', (e) => {
        console.error(e);
        return "error";
    });
}

// function toDataUrl(url, callback) {
//     console.log("toDataUrl #1");
//     var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
//     var FileReader = require('filereader')
//     console.log("toDataUrl #2");

//     var xhr = new XMLHttpRequest();
//     xhr.onload = function () {
//         console.log("toDataUrl #3");
//         var reader = new FileReader();
//         reader.onloadend = function () {
//             console.log("toDataUrl #4");
//             callback(reader.result);
//         }
//         reader.onerror = function () {
//             console.log("An error occured in reader");
//         }
//         console.log("xhr.respone = " + xhr.response);
//         reader.readAsDataURL(xhr.response);
//     };
//     xhr.onerror = function () {
//         console.log("An error occured during the transaction.");
//     }
//     console.log("toDataUrl #5 url = " + url);
//     xhr.open('GET', url);
//     xhr.responseType = 'blob';
//     xhr.send();
// }

export async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}