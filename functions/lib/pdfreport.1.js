"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { getPDF } from './genpdf';
const pdf = require('./genpdf');
const https = require('https');
const admin = require('firebase-admin');
const fs = require('fs');
const archiver = require('archiver');
const PDFDocument = require('pdfkit');
exports.pdfReport = function (request, response) {
    return __awaiter(this, void 0, void 0, function* () {
        var archive = archiver('zip', {
            zlib: { level: 9 } // Sets the compression level.
        });
        // listen for all archive data to be written
        // 'close' event is fired only when a file descriptor is involved
        response.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');
        });
        // This event is fired when the data source is drained no matter what was the data source.
        // It is not part of this library but rather from the NodeJS Stream API.
        // @see: https://nodejs.org/api/stream.html#stream_event_end
        response.on('end', function () {
            console.log('Data has been drained');
        });
        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function (err) {
            if (err.code === 'ENOENT') {
                // log warning
            }
            else {
                // throw error
                throw err;
            }
        });
        // good practice to catch this error explicitly
        archive.on('error', function (err) {
            throw err;
        });
        const projectKey = request.query.projectKey;
        const assets = yield admin.firestore().collection('assets').where('project_key', '==', projectKey);
        let formKeys = [];
        yield assets.get().then((o) => __awaiter(this, void 0, void 0, function* () {
            for (let i = 0; i < o.docs.length; i++) {
                const assetKey = o.docs[i].data()['$key'];
                const forms = yield admin.firestore().collection('forms').where('asset_key', '==', assetKey);
                yield forms.get().then((p) => __awaiter(this, void 0, void 0, function* () {
                    for (let j = 0; j < p.docs.length; j++) {
                        const formKey = p.docs[j].data()['$key'];
                        formKeys.push(formKey);
                        console.log("processing form = " + formKey);
                        yield pdf.getPDF(formKey, archive);
                    }
                })).catch(e => {
                    console.log(e);
                });
            }
        }));
        // pipe archive data to the response
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader('Content-Disposition', 'attachment; filename=test.zip');
        archive.pipe(response);
        setTimeout(() => {
            archive.finalize();
        }, 500);
    });
};
//# sourceMappingURL=pdfreport.1.js.map