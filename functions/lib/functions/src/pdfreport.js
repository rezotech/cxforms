"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const https = require('https');
admin.initializeApp(functions.config().firebase);
const PDFDocument = require('pdfkit');
exports.pdfReport = function (request, response) {
    var AdmZip = require('adm-zip');
    var zip = new AdmZip();
    const bucket = admin.storage().bucket("cxforms-6dcec.appspot.com");
    const text = request.query.text;
    const docRef = admin.firestore().collection("forms").doc(text);
    this.assets = admin.firestore().collection('assets', ref => ref.where('project_key', "==", project.$key)).valueChanges();
    response.send("pdfReport");
};
//# sourceMappingURL=pdfreport.js.map