"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const PDFDocument = require("pdfkit");
admin.initializeApp(functions.config().firebase);
// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
// apiKey: "AIzaSyCBsD2iip1a3ECa8Ci_ZoxZ_AmYRQbq_EE",
// authDomain: "cxforms-6dcec.firebaseapp.com",
// databaseURL: "https://cxforms-6dcec.firebaseio.com",
// projectId: "cxforms-6dcec",
// storageBucket: "",
// messagingSenderId: "219824614931"
exports.helloWorld = functions.https.onRequest((request, response) => {
    const bucket = admin.storage().bucket("cxforms-6dcec.appspot.com");
    const filename = "test.pdf";
    const file = bucket.file(filename);
    const bucketFileStream = file.createWriteStream();
    const buffers = [];
    const doc = new PDFDocument();
    const p = new Promise((resolve, reject) => {
        doc.on("end", function () {
            resolve(buffers);
        });
        doc.on("error", function () {
            reject();
        });
    });
    doc.pipe(bucketFileStream);
    doc.on('data', buffers.push.bind(buffers));
    doc.addPage();
    doc.text("What up?");
    doc.end();
    response.send("Hello from Firebase!");
    // return p.then(function(buffers) {
    //     console.log("do sendmail!");
    // }); 
});
exports.genPdf = functions.https.onRequest((request, response) => {
    const bucket = admin.storage().bucket("cxforms-6dcec.appspot.com");
    const filename = "test.pdf";
    const file = bucket.file(filename);
    const bucketFileStream = file.createWriteStream();
    const buffers = [];
    const text = request.query.text;
    const pdfdoc = new PDFDocument();
    //doc.pipe(bucketFileStream);
    pdfdoc.pipe(response);
    //doc.on('data', buffers.push.bind(buffers));
    const docRef = admin.firestore().collection("cxforms").doc(text);
    docRef.get().then(function (doc) {
        if (doc.exists) {
            const cxform = doc.data();
            pdfdoc.text("Test Print 23 " + text);
            pdfdoc.end();
        }
        else {
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error gettting documents: ", error);
    });
    // response.send("Hello from Firebase " + text + " response = " + file.getSignedUrl());
});
//# sourceMappingURL=index.js.map