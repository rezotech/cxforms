"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const https = require('https');
function toBase64(cellEl, x, y, pdfdoc) {
    console.log("toBase64(url) #1");
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        console.log("toBase64(url) in Promise #2");
        const req = yield https.get(cellEl.url, (res) => {
            res.setEncoding('base64');
            let body = "data:" + res.headers["content-type"] + ";base64,";
            // console.log('statusCode:', res.statusCode);
            // console.log('headers:', res.headers);
            res.on('data', (d) => {
                // console.log('on data');
                body += d;
            });
            res.on('end', () => {
                console.log('toBase64(url) end body = ' + body);
                pdfdoc.image(body, x, y, { scale: .7 });
                resolve(res);
            });
        });
        req.on('error', err => {
            console.error('error!');
            reject(err);
        });
    }));
}
exports.toBase64 = toBase64;
function toBase642(url) {
    return __awaiter(this, void 0, void 0, function* () {
        let body = "";
        const getReq = yield https.get(url, (res) => __awaiter(this, void 0, void 0, function* () {
            res.setEncoding('base64');
            body = "data:" + res.headers["content-type"] + ";base64,";
            console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);
            res.on('data', (d) => {
                console.log('on data');
                body += d;
            });
            res.on('end', () => {
                console.log('body = ' + body);
                return body;
            });
        }));
        getReq.end();
        getReq.on('error', (e) => {
            console.error(e);
            return "error";
        });
    });
}
exports.toBase642 = toBase642;
// function toDataUrl(url, callback) {
//     console.log("toDataUrl #1");
//     var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
//     var FileReader = require('filereader')
//     console.log("toDataUrl #2");
//     var xhr = new XMLHttpRequest();
//     xhr.onload = function () {
//         console.log("toDataUrl #3");
//         var reader = new FileReader();
//         reader.onloadend = function () {
//             console.log("toDataUrl #4");
//             callback(reader.result);
//         }
//         reader.onerror = function () {
//             console.log("An error occured in reader");
//         }
//         console.log("xhr.respone = " + xhr.response);
//         reader.readAsDataURL(xhr.response);
//     };
//     xhr.onerror = function () {
//         console.log("An error occured during the transaction.");
//     }
//     console.log("toDataUrl #5 url = " + url);
//     xhr.open('GET', url);
//     xhr.responseType = 'blob';
//     xhr.send();
// }
function asyncForEach(array, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let index = 0; index < array.length; index++) {
            yield callback(array[index], index, array);
        }
    });
}
exports.asyncForEach = asyncForEach;
//# sourceMappingURL=common.js.map