"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const genpdf_1 = require("./genpdf");
const testing_1 = require("./testing");
const pdfreport_1 = require("./pdfreport");
const pdfrefreport_1 = require("./pdfrefreport");
const https = require('https');
admin.initializeApp(functions.config().firebase);
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Welcome to the world!");
});
exports.generatePDF = functions.https.onRequest(genpdf_1.genPDF);
exports.pdfRef = functions.https.onRequest(genpdf_1.genrPDF);
exports.test = functions.https.onRequest(testing_1.testingExporting);
exports.genReport = functions.https.onRequest(pdfreport_1.pdfReport);
exports.genrefReport = functions.https.onRequest(pdfrefreport_1.pdfrefReport);
function transferFailed(evt) {
    console.log("An error occured.");
}
function transferComplete(evt) {
    console.log("The transfer is complete.");
}
//# sourceMappingURL=index.js.map