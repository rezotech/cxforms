import { Component, OnInit, ViewChild, ComponentFactoryResolver, Inject } from '@angular/core';
import { ToolbarService } from '../toolbar/toolbar.service';
import { ElementRef } from '@angular/core';
import { ViewContainerRef } from '@angular/core';
import { ScService } from '../sc.service';
import { CxformComponent } from '../cxforms/cxform/cxform.component';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-mainwin',
  templateUrl: './mainwin.component.html',
  styleUrls: ['./mainwin.component.css']
})
export class MainwinComponent {
  factoryResolver: ComponentFactoryResolver;
  @ViewChild("mainsplit", { read: ElementRef }) mainsplitEl: ElementRef;
  @ViewChild("assetlist", { read: ElementRef }) assetlistEl: ElementRef;
  @ViewChild("formsview", { read: ElementRef }) formsviewEl: ElementRef;
  @ViewChild("formrefsview", { read: ElementRef }) formrefsviewEl: ElementRef;
 
  contextmenu = false;
  contextmenuX = 0;
  contextmenuY = 0;
  inner: boolean = false;

  constructor(
    @Inject(ComponentFactoryResolver) factoryResolver,
    private toolbarService: ToolbarService,
    formContainerRef: ViewContainerRef,
    private scService: ScService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.factoryResolver = factoryResolver;

    this.route.params.subscribe(params => {
      if(params['id']) {
        // console.log("Doing a form!")
        if (params['id']) {
          this.toolbarService.getFormbyId(params['id']);
        }
      } else {
        // console.log("Doing a formref!");
      }
      let key1 = params['id1'];
      // console.log('key1 = ' + key1);
      
      let key2 = params['id2'];
      // console.log('key2 = ' + key2);
      if (key2) {
        this.toolbarService.getFormbyId(key2);
      }
    });

    this.scService.cxForm_messageSource.subscribe((cxForm => {
      if (cxForm == null) {
        // console.log("cxForm == null");
        return;
      }
      // console.log("cxForm = " + cxForm.name); 
    }));

    this.scService.mode_messageSource.subscribe((formsMode => {
      if(!this.assetlistEl) {
        return;
      }
      if (formsMode == "form") {
        // console.log("formsMode == form");
        this.formsviewEl.nativeElement.style.display = "block";
        this.formrefsviewEl.nativeElement.style.display = "none";
        this.assetlistEl.nativeElement.style.display = "none";
      } else if (formsMode == "formref") {
        this.formsviewEl.nativeElement.style.display = "none";
        this.formrefsviewEl.nativeElement.style.display = "block";
        this.assetlistEl.nativeElement.style.display = "none";
      } else if (formsMode == "asset") {
        this.formsviewEl.nativeElement.style.display = "none";
        this.formrefsviewEl.nativeElement.style.display = "none";
        this.assetlistEl.nativeElement.style.display = "block";
      }
      // console.log("cxForm = " + cxForm.name); 
    }));

  }

  onrightClick(event) {
    this.contextmenuX = (event.clientX);
    this.contextmenuY = (event.clientY);
    // console.log("screenX,Y = " + event.screenX + ", " + event.screenY);
    // console.log("clientX,Y = " + event.clientX + ", " + event.clientY);
    this.inner = this.scService.getiMerge();
    this.contextmenu = true;
  }
  disableContextMenu() {
    this.contextmenu = false;
  }
  refresh() {
    let scrollTop = this.mainsplitEl.nativeElement.scrollTop;
    let height = this.mainsplitEl.nativeElement.clientHeight;
    console.log("scrollTop = " + scrollTop);
    console.log("height = " + height);
  }
  ngOnInit() {
    // if(this.scService.getDeviceType()!="unknown") {
    //   this.router.navigate(['/mobilemain']);
    //   return;
    // }
   
    var formsMode = this.scService.getCurrMode();
    if (formsMode == "form") {
      // console.log("formsMode == form");
      this.formsviewEl.nativeElement.style.display = "block";
      this.formrefsviewEl.nativeElement.style.display = "none";
      this.assetlistEl.nativeElement.style.display = "none";
    } else if (formsMode == "formref") {
      this.formsviewEl.nativeElement.style.display = "none";
      this.formrefsviewEl.nativeElement.style.display = "block";
      this.assetlistEl.nativeElement.style.display = "none";
    } else if (formsMode == "asset") {
      this.formsviewEl.nativeElement.style.display = "none";
      this.formrefsviewEl.nativeElement.style.display = "none";
      this.assetlistEl.nativeElement.style.display = "block";
    }
  }

}
