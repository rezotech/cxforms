import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainwinComponent } from './mainwin.component';

describe('MainwinComponent', () => {
  let component: MainwinComponent;
  let fixture: ComponentFixture<MainwinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainwinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainwinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
