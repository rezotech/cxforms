import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ScService } from '../../sc.service';
import { Router } from '@angular/router';
import { Project } from '../../cxforms/shared/project.model';
import { Asset } from '../../cxforms/shared/asset.model';
import { Cxform } from '../../cxforms/shared/cxform.model';
import { Page } from '../../cxforms/shared/page.model';

@Component({
  selector: 'app-accountadmin',
  templateUrl: './accountadmin.component.html',
  styleUrls: ['./accountadmin.component.css']
})
export class AccountadminComponent implements OnInit {
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
  assetId: string;
  local_pdfUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/genReport?projectKey=";
  serve_pdfUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/genReport?projectKey=";
  pdfUrl: string = this.local_pdfUrl;

  constructor(
    private afs: AngularFirestore,
    private scService: ScService,
    private router: Router
  ) {
    this.scService.assetment_messageSource.subscribe(asset => {
      if(asset==null) {
        return;
      }
      this.assetId = asset.$key;
    });
  }
}
