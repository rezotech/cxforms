import { Injectable, OnInit, NgZone } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { DoundoService } from '../command/doundo.service';

import { Subject } from 'rxjs/Subject';

import { Cxform } from '../cxforms/shared/cxform.model';
import { Page } from '../cxforms/shared/page.model';
import { Table } from '../cxforms/shared/table.model';
import { Row } from '../cxforms/shared/row.model';
import { Cell } from '../cxforms/shared/cell.model';
import { ScService } from '../sc.service';
import { CellComponent } from '../cxforms/cxform/table/cell/cell.component';
import { Asset } from '../cxforms/shared/asset.model';
import { Project } from '../cxforms/shared/project.model';
import { Formref } from '../cxforms/shared/formref.model';
import { Assetref } from '../cxforms/shared/assetref.model';


@Injectable()
export class ToolbarService {
  cxformCollection: AngularFirestoreCollection<Cxform>;
  pagesCollection: AngularFirestoreCollection<Page>;
  cxformDoc: AngularFirestoreDocument<Cxform>;
  cxForms: Observable<Cxform[]>;
  cxForm: Observable<Cxform>;
  form: Cxform;
  private subject = new Subject<any>();

  constructor(
    private zone: NgZone,
    private afs: AngularFirestore,
    private doundo: DoundoService,
    private scService: ScService
  ) {
    this.zone.runOutsideAngular(() => {
      // console.log("updatePage2()!");
      this.pagesCollection = this.afs.collection('pages');
    });
   }

  getCxforms() {
    this.cxformCollection = this.afs.collection('forms');
    this.cxForms = this.cxformCollection.snapshotChanges().map(action => {
      return action.map(a => {
        const data = a.payload.doc.data() as Cxform;
        data.$key = a.payload.doc.id;
        return { ...data };
      });
    });
    return this.cxForms;
  }

  getFormbyId(id: string) {
    // console.log("getFormbyId() #1");
    this.cxformDoc = this.afs.doc<Cxform>('forms/' + id);
    this.cxformDoc.snapshotChanges().map(a => {
      // console.log("Here I am!" + a);
    });
  }

  createForm(cxform: Cxform): null {
    let formId = this.afs.createId();
    var formRef = this.afs.collection('forms').doc(formId);
    cxform.$key = formId;
    formRef.set(JSON.parse(JSON.stringify(cxform)));
    var page = new Page();
    page.formKey = formId;
    page.pageno = 1;
    this.afs.collection('pages').add(JSON.parse(JSON.stringify(page)));
    return;
  }

  updateCxform(cxform: Cxform) {
    this.afs.collection('forms').doc(cxform.$key).set(JSON.parse(JSON.stringify(cxform)));
  }

  deleteCxform($key: string) {
    // console.log("delete key = " + $key);
    var pagesCollection: AngularFirestoreCollection<Page>;
    pagesCollection = this.afs.collection<Page>('pages', ref => ref.where('formKey', '==', $key).orderBy('pageno'));
    pagesCollection.snapshotChanges().subscribe(snapshot => {
      snapshot.forEach(childSnapshot => {
        this.afs.collection('pages').doc(childSnapshot.payload.doc.id).delete();
      });
    });
    this.afs.collection('forms').doc($key).delete();
  }

  createAssetef(_assetref: Assetref): null {
    let assetRefId = this.afs.createId();
    var assetRef = this.afs.collection('assetrefs').doc(assetRefId);
    _assetref.$key = assetRefId;
    assetRef.set(JSON.parse(JSON.stringify(_assetref)));
    return;
  }

  updateAssetref(assetref: Assetref) {
    this.afs.collection('assetrefs').doc(assetref.$key).set(JSON.parse(JSON.stringify(assetref)));
  }

  deleteAssetref($key: string) {
    // console.log("delete key = " + $key);
    this.afs.collection('assetrefs').doc($key).delete();
  }

  createFormref(_formref: Formref): null {
    let formRefId = this.afs.createId();
    var formRef = this.afs.collection('formrefs').doc(formRefId);
    _formref.$key = formRefId;
    formRef.set(JSON.parse(JSON.stringify(_formref)));
    return;
  }

  updateFormref(formref: Formref) {
    this.afs.collection('formrefs').doc(formref.$key).set(JSON.parse(JSON.stringify(formref)));
  }

  deleteFormref($key: string) {
    // console.log("delete key = " + $key);
    this.afs.collection('formrefs').doc($key).delete();
  }

  // addCxPage() {
  //   this._mainInjService.addDynamicComponent();
  // }

  updateAsset(asset: Asset) {
    this.afs.collection('assets').doc(asset.$key).set(JSON.parse(JSON.stringify(asset)));
  }

  updateProject(project: Project) {
    this.afs.collection('projects').doc(project.$key).set(JSON.parse(JSON.stringify(project)));
  }

  insertPage(page: Page): string {
    // console.log("Toolbar.service adding a page");
    let pageId = this.afs.createId();
    var pageRef = this.afs.collection('pages').doc(pageId);
    pageRef.set(JSON.parse(JSON.stringify(page)));
    return pageId;
  }

  updatePage(page: Page) {
    console.log("updatePage1()! key = " + page.$key);
    this.afs.collection('pages').doc(page.$key).set(JSON.parse(JSON.stringify(page)));
    //this.pagesCollection.doc(page.$key).set(JSON.parse(JSON.stringify(page)));
    // this.zone.runOutsideAngular(() => {
    //   console.log("updatePage2()!");
    //   this.afs.collection('pages').doc(page.$key).set(JSON.parse(JSON.stringify(page)));
    // });
  }

  getPages($key: string) {
    // console.log("get pages by key = " + $key);
    return this.afs.collection('pages').doc($key);
  }

  deletePage($key: string) {
    // console.log("delete page key = " + $key);
    this.afs.collection('pages').doc($key).delete();
  }

  doMerge() {
    // console.log("doMerge()");
    let range: CellComponent[];
    range = this.scService.getCellRange();
    // console.log("doMerge colspan = " + this.scService.getColSpan());
    // console.log("doMerge rowspan = " + this.scService.getRowSpan());
    let height: number = 0;
    let width: number = 0;
    for (var r = range[0].cell.row; r < (range[0].cell.row + this.scService.getRowSpan()); r++) {
      height = height + this.scService.getCurrTableComp().table.rows[r].height;
    }
    for (var c = range[0].cell.col; c < (range[0].cell.col + this.scService.getColSpan()); c++) {
      width = width + this.scService.getCurrTableComp().table.rows[0].cells[c].width;
    }
    for (var i = 0; i < range.length; i++) {
      if (i == 0) {
        range[i].cell.colspan = this.scService.getColSpan();
        range[i].cell.rowspan = this.scService.getRowSpan();
        range[i].cell.height = height;
        range[i].cell.width = width;
      } else {
        range[i].cell.display = "none";
      }
    }
    this.updatePage(this.scService.getCurrPageComp().page);
  }

  doUnMerge() {
    // console.log("doUnMerge()");
    let cellComp = this.scService.getCellRange()[0];
    let tableComp = this.scService.getCurrTableComp();
    tableComp.unMerge(cellComp.cell.row, cellComp.cell.col, cellComp.cell.rowspan, cellComp.cell.colspan);
    this.updatePage(this.scService.getCurrPageComp().page);
  }

  doiMerge() {
    // console.log("doiMerge()");
    let irange: CellComponent[];
    irange = this.scService.getiCellRange();
    // console.log("doMerge colspan = " + this.scService.getiColSpan());
    // console.log("doMerge rowspan = " + this.scService.getiRowSpan());
    for (var i = 0; i < irange.length; i++) {
      if (i == 0) {
        irange[i].cell.colspan = this.scService.getiColSpan();
        irange[i].cell.rowspan = this.scService.getiRowSpan();
      } else {
        irange[i].cell.display = "none";
      }
    }
    this.updatePage(this.scService.getCurrPageComp().page);
  }

  doiUnMerge() {
    // console.log("doiUnMerge()");
    let icellComp = this.scService.getiCellRange()[0];
    let itableComp = this.scService.getCurrITableComp();
    itableComp.unMerge(icellComp.cell.row, icellComp.cell.col, icellComp.cell.rowspan, icellComp.cell.colspan);
    this.updatePage(this.scService.getCurrPageComp().page);
  }

}