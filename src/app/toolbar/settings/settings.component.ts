import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ScService } from '../../sc.service';
import { ToolbarService } from '../../toolbar/toolbar.service';
import { CellComponent } from '../../cxforms/cxform/table/cell/cell.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  @ViewChild("tblInput", { read: ElementRef }) tblInputEl: ElementRef;
  @ViewChild("tblText", { read: ElementRef }) tblTextEl: ElementRef;
  @ViewChild("tblForm", { read: ElementRef }) tblFormEl: ElementRef;
  @ViewChild("tblPage", { read: ElementRef }) tblPageEl: ElementRef;
  @ViewChild("tblTable", { read: ElementRef }) tblTableEl: ElementRef;
  @ViewChild("tblCell", { read: ElementRef }) cellFormEl: ElementRef;
  @ViewChild("tbliCell", { read: ElementRef }) icellFormEl: ElementRef;
  @ViewChild("tblITable", { read: ElementRef }) tblITableEl: ElementRef;
  @ViewChild("tblHr", { read: ElementRef }) tblHrEl: ElementRef;
  form_name: string = "Form Name";
  col_width: number;
  row_height: number;
  icol_width: number;
  irow_height: number;
  input_width: number;
  input_height: number;
  text_value: string;
  text_ftsize: number;
  hr_width: number;
  hr_size: number;
  enable_input: boolean = false;
  enable_text: boolean = false;
  enable_form: boolean = false;
  enable_page: boolean = false;
  enable_table: boolean = false;
  enable_cell: boolean = false;
  enable_itable: boolean = false;
  enable_hr: boolean = false;

  constructor(private scService: ScService, private toolbarservice: ToolbarService) {

    this.scService.cxForm_messageSource.subscribe((form => {
      if (form == null) {
        return;
      } else {
        this.form_name = form.name;
      }
    }));

    this.scService.page_messageSource.subscribe((page => {
      if (page == null) {
        return;
      }
      // console.log("page = " + page.page.pageno);
    }));

    this.scService.table_messageSource.subscribe((table => {
      // console.log("ontable change()");
      if (table == null) {
        return;
      }
      let cell = this.scService.getCurrCellComp();
      if (cell) {
        //      console.log("cell = " + cell.cell.row + ", " + cell.cell.col);
        let width = table.table.cols[cell.cell.col];
        //      console.log("table change col width = " + width);
        this.col_width = width;
      }
    }));

    this.scService.itable_messageSource.subscribe((itable => {
      if (itable == null) {
        return;
      }
      let cell = this.scService.getCurrICell();
      if (cell) {
        let width = itable.cellel.table.cols[cell.cell.col];
        let height = itable.cellel.table.rows[cell.cell.row].height;
        this.icol_width = width;
        this.irow_height = height;
      }
    }));

    this.scService.row_messageSource.subscribe((row => {
      //      console.log("onrow change()");
      if (row == null) {
        return;
      }
      this.row_height = row.height;
    }));

    this.scService.cell_messageSource.subscribe((cell => {
      //      console.log("oncell change()");
      if (cell == null) {
        return;
      }
      // if (this.scService.getCurrTableComp()) {
        //        console.log("cell = " + cell.cell.row + ", " + cell.cell.col);
        let width = this.scService.getCurrTableComp().table.cols[cell.col];
        //        console.log("cell change col width = " + width);
        this.col_width = this.scService.getCurrTableComp().table.cols[cell.col];
      // }
    }));

    this.scService.cellel_messageSource.subscribe((cellel => {
      //      console.log("oncell change()");
      if (cellel == null) {
        return;
      }
      
      if(cellel.type=="Text") { // window.alert("onChange() " + cellel.text);
        // window.alert(cellel.text);
        this.text_value = cellel.text;
        this.text_ftsize = cellel.ftsize;
      } else if(cellel.type=="Input") {
        this.input_width = cellel.width;
        this.input_height = cellel.height;
      } else if(cellel.type=="HR") {
        this.hr_width = cellel.width;
        this.hr_size = cellel.size;
      }
     
    }));
  }

  iwidthBlur(event: any) {
    //window.alert("event = " + event.target.value);
    if(this.scService.getCurrCellEl().type=="Input") {
      this.scService.getCurrCellEl().width = event.target.value;
      this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
    }
  }

  iheightBlur(event: any) {
    //window.alert("event = " + event.target.value);
    if(this.scService.getCurrCellEl().type=="Input") {
      this.scService.getCurrCellEl().height = event.target.value;
      this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
    }
  }

  ftSizeValueBlur(event: any) {
    var value: number;
    value += event.target.value;
    if(!isNaN(event.target.value)) {
      console.log("is a number!");
      if(event.target.value<100 && event.target.value>0) {
        console.log("GOOD!");
        this.scService.getCurrCellEl().ftsize = event.target.value;
        this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
      } else {
        console.log("NOT GOOD!");
      }
    } else {
      console.log("not a number!")
    }
  }

  fnameBlur(event: any) {
    var value: string = "";
    value += event.target.value;
    console.log("value = " + value);
    this.scService.getCxForm().name = value;
    this.toolbarservice.updateCxform(this.scService.getCxForm());
  }
  colwidthBlur(event: any) {
    var value;
    value = event.target.value;
    if(value<25) {
      return;
    }
    console.log("value = " + value);
    var col = this.scService.getCurrCellComp().cell.col;
    console.log("col = " + col);
    for(var i=0; i<this.scService.getCurrTableComp().table.rows.length; i++) {
      this.scService.getCurrTableComp().table.rows[i].cells[col].width = +value;
    }
    this.scService.getCurrTableComp().table.cols[col] = +value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }
  rowheightBlur(event: any) {
    var value;
    value = event.target.value;
    if(value<25) {
      return;
    }
    console.log("value = " + value);
    var height = this.scService.getCurrRow().height;
    console.log("height = " + height);
    for(var i=0; i<this.scService.getCurrTableComp().table.cols.length; i++) {
      this.scService.getCurrRow().cells[i].height = +value;
    }
    this.scService.getCurrRow().height = +value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }
  icolwidthBlur(event: any) {
    var value;
    value = event.target.value;
    if(value<25) {
      return;
    }
    console.log("value = " + value);
    var col = this.scService.getCurrICell().cell.col;
    console.log("col = " + col);
    console.log("table rows = " + this.scService.getCurrITableComp().cellel.type);
    for(var i=0; i<this.scService.getCurrITableComp().cellel.table.rows.length; i++) {
      this.scService.getCurrITableComp().cellel.table.rows[i].cells[col].width = +value;
    }
    this.scService.getCurrITableComp().cellel.table.cols[col] = +value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }
  irowheightBlur(event: any) {
    var value;
    value = event.target.value;
    if(value<25) {
      return;
    }
    console.log("value = " + value);
    var row = this.scService.getCurrITableComp().cellel.table.rows[this.scService.getCurrICell().cell.row];
    var cell = this.scService.getCurrICell().cell;
    console.log("height = " + cell.height);
    row.height = +value;
    cell.height = +value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }
  textValueBlur(event: any) {
    this.scService.getCurrCellEl().text = event.target.value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
    // this.text_value = "";
    // this.text_ftsize = 0;
  }
  hrwidthBlur(event: any) {
    var value;
    value = event.target.value;
    if(value<25) {
      return;
    }
    console.log("value = " + value);
    var cellel = this.scService.getCurrCellEl();
    cellel.width = value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }
  hrsizeBlur(event: any) {
    var value;
    value = event.target.value;
    if(value<1) {
      return;
    }
    console.log("value = " + value);
    var cellel = this.scService.getCurrCellEl();
    cellel.size = value;
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }
  tglInput(e: Event) {
    if(this.enable_input) {
      this.tblInputEl.nativeElement.style.display = "none";
      this.enable_input = false;
    } else {
      this.tblInputEl.nativeElement.style.display = "block";
      this.enable_input = true;
    }
  }
  tglText(e: Event) {
    if(this.enable_text) {
      this.tblTextEl.nativeElement.style.display = "none";
      this.enable_text = false;
    } else {
      this.tblTextEl.nativeElement.style.display = "block";
      this.enable_text = true;
    }
  }
  tglITable(e: Event) {
    if(this.enable_itable) {
      this.tblITableEl.nativeElement.style.display = "none";
      this.enable_itable = false;
    } else {
      this.tblITableEl.nativeElement.style.display = "block";
      this.enable_itable = true;
    }
  }
  tglForm(e: Event) {
    if(this.enable_form) {
      this.tblFormEl.nativeElement.style.display = "none";
      this.enable_form = false;
    } else {
      this.tblFormEl.nativeElement.style.display = "block";
      this.enable_form = true;
    }
  }
  tglPage(e: Event) {
    if(this.enable_page) {
      this.tblPageEl.nativeElement.style.display = "none";
      this.enable_page = false;
    } else {
      this.tblPageEl.nativeElement.style.display = "block";
      this.enable_page = true;
    }
  }
  tglTable(e: Event) {
    if(this.enable_table) {
      this.tblTableEl.nativeElement.style.display = "none";
      this.enable_table = false;
    } else {
      this.tblTableEl.nativeElement.style.display = "block";
      this.enable_table = true;
    }
  }
  tglCell(e: Event) {
    if(this.enable_cell) {
      this.cellFormEl.nativeElement.style.display = "none";
      this.enable_cell = false;
    } else {
      this.cellFormEl.nativeElement.style.display = "block";
      this.enable_cell = true;
    }
  }
  tglHr(e: Event) {
    if(this.enable_hr) {
      this.tblHrEl.nativeElement.style.display = "none";
      this.enable_hr = false;
    } else {
      this.tblHrEl.nativeElement.style.display = "block";
      this.enable_hr = true;
    }
  }
  tgliCell(e: Event) {
    if(this.enable_cell) {
      this.icellFormEl.nativeElement.style.display = "none";
      this.enable_cell = false;
    } else {
      this.icellFormEl.nativeElement.style.display = "block";
      this.enable_cell = true;
    }
  }

  alignLeft(e: Event) {
    this.scService.getCurrCellComp().cell.halign="Left";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  alignCenter(e: Event) {
    this.scService.getCurrCellComp().cell.halign="Center";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  alignRight(e: Event) {
    this.scService.getCurrCellComp().cell.halign="Right";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  alignBottom(e: Event) {
    this.scService.getCurrCellComp().cell.valign="Bottom";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  alignMiddle(e: Event) {
    this.scService.getCurrCellComp().cell.valign="Middle";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  alignTop(e: Event) {
    this.scService.getCurrCellComp().cell.valign="Top";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  ialignLeft(e: Event) {
    this.scService.getCurrICell().cell.halign="Left";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  ialignCenter(e: Event) {
    this.scService.getCurrICell().cell.halign="Center";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  ialignRight(e: Event) {
    this.scService.getCurrICell().cell.halign="Right";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  ialignBottom(e: Event) {
    this.scService.getCurrICell().cell.valign="Bottom";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  ialignMiddle(e: Event) {
    this.scService.getCurrICell().cell.valign="Middle";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  ialignTop(e: Event) {
    this.scService.getCurrICell().cell.valign="Top";
    this.toolbarservice.updatePage(this.scService.getCurrPageComp().page);
  }

  onNoClick(): void {
  }

  ngOnInit() {
  }

}
