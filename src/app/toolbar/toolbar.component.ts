import { Component, Inject, ViewChild, ElementRef } from '@angular/core';

import * as firebase from 'firebase/app';

import { Cxform } from '../cxforms/shared/cxform.model';
import { Page } from '../cxforms/shared/page.model';
import { Table } from '../cxforms/shared/table.model';
import { Row } from '../cxforms/shared/row.model';
import { Cell } from '../cxforms/shared/cell.model';
import { CxformComponent } from '../cxforms/cxform/cxform.component';
import { SettingsComponent } from './settings/settings.component';
import { Router } from '@angular/router';

import { ToolbarService } from './toolbar.service';
import { ScService } from '../sc.service';
import { DoundoService } from '../command/doundo.service';

import { AddPage } from '../command/pages/add-page';
import { Cellel } from '../cxforms/shared/cellel.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { Formref } from '../cxforms/shared/formref.model';
import { AngularFirestore } from 'angularfire2/firestore';
import { Project } from '../cxforms/shared/project.model';
import { Asset } from '../cxforms/shared/asset.model';


@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html'
})
export class ToolbarComponent {
  @ViewChild("tglbtn", { read: ElementRef }) tglbtnEl: ElementRef;
  user_name: string;
  form_name: string;
  animal: string;
  name: string;
  local_pdfUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/genReport?projectKey=";
  serve_pdfUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/genReport?projectKey=";
  pdfUrl: string = this.local_pdfUrl;

  local_pdfrefUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/genrefReport?projectKey=";
  serve_pdfrefUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/genrefReport?projectKey=";
  pdfrefUrl: string = this.local_pdfrefUrl;

  constructor(
    private afs: AngularFirestore,
    private toolbarService: ToolbarService,
    private scService: ScService,
    private doundoService: DoundoService,
    private router: Router,
    public afAuth: AngularFireAuth
  ) {
    this.scService.user_messageSource.subscribe((user => {
      if (user != null) {
        this.user_name = user.email;
      }
    }));
    this.scService.cxForm_messageSource.subscribe((cxForm => {
      if (cxForm != null) {
        // console.log("cxForm = " + cxForm.name);
        this.form_name = cxForm.$key;
      }
    }));

  }

  fireOver(e) {
    // console.log(e)
  }
  fireDown(e) {
    window.alert("Mouse Dn!")
    // console.log("fire down 3 : " + e);
    var cxForm = new Cxform();
    //cxForm.pages = new Array();
    var page = new Page();
    page.pageno = 1;
    //cxForm.pages.push(page);
    cxForm.name = "What up?";
    this.toolbarService.createForm(cxForm)
    window.alert("key = " + cxForm.$key);
  }
  listForms(e) {
    // this.toolbarService.getData().valueChanges().subscribe(console.log);
  }
  fireMove(e) {
    // console.log("fire move : " + e)
  }
  delForm(e) {
    this.toolbarService.deleteCxform(this.scService.getCxForm().$key);
  }
  addFormref(e) {
    var formref = new Formref();
    formref.form_key = this.scService.getCxForm().$key;
    this.toolbarService.createFormref(formref);

  }
  delFormref(e) {
    this.toolbarService.deleteFormref(this.scService.getCurrFormref().$key);
  }
  addPage(e: Event) {
    // console.log("addPage");
    var addPage = new AddPage();
    addPage.toolbarService = this.toolbarService;
    addPage.scService = this.scService;
    addPage.router = this.router;
    addPage.form = this.scService.getCxForm();
    addPage.do();
    this.doundoService.addCommand(addPage);

  }
  delPage(e) {
    this.toolbarService.deletePage(this.scService.getCurrPageComp().page.$key);
    this.scService.getCxForm().no_pages = (this.scService.getCxForm().no_pages - 1);
    this.toolbarService.updateCxform(this.scService.getCxForm());
  }
  addTable(e) {
    var table = new Table();
    table.cols.push(700);
    var row = new Row();
    row.row = 0;
    row.height = 25;
    table.rows.push(row);
    var cell = new Cell();
    cell.row = row.row;
    cell.col = 0;
    cell.width = 700;
    cell.height = 25;
    cell.colspan = 1;
    cell.rowspan = 1;
    cell.halign = "Center";
    cell.valign = "Middle";
    row.cells.push(cell);
    this.scService.getCurrPageComp().page.tables.push(table);
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }
  delTable(e) {
    const indexNo = this.scService.getCurrPageComp().page.tables.indexOf(this.scService.getCurrTableComp().table);
    this.scService.getCurrPageComp().page.tables.splice(indexNo,1);
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }
  addRow(e) {
    this.scService.getCurrCxformComp().attachChanges();
    var row = new Row();
    var table = this.scService.getCurrTableComp().table;
    row.row = table.rows.length;
    row.height = 25;
    table.rows.push(row);

    for (var i = 0; i < table.cols.length; i++) {
      var cell = new Cell();
      cell.width = table.cols[i];
      cell.height = 25;
      cell.row = row.row;
      cell.col = row.cells.length;
      cell.colspan = 1;
      cell.rowspan = 1;
      cell.halign = "Center";
      cell.valign = "Middle";
      row.cells.push(cell);
    }
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }
  delRow(e) {
    const indexNo =this.scService.getCurrCellComp().cell.row;
    this.scService.getCurrTableComp().table.rows.splice(indexNo,1);
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
  }
  delCell(e) {
    this.scService.getCurrCellComp().cell.cellels.length = 0;
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
  }
  addCol(e) {
    var table = this.scService.getCurrTableComp().table;
    table.cols.push(100);
    for (var i = 0; i < table.rows.length; i++) {
      var cell = new Cell();
      cell.row = table.rows[i].row;
      cell.col = table.rows[i].cells.length;
      cell.colspan = 1;
      cell.rowspan = 1;
      cell.width = 100;
      cell.height = table.rows[i].height;
      cell.halign = "Center";
      cell.valign = "Middle";
      table.rows[i].cells.push(cell);
    }
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
  }

  delCol(e) {
  }

  celCell(e) {

  }

  addText(e) {
    var cell = this.scService.getCurrCellComp().cell;
    var cellel = new Cellel();
    cellel.type = "Text";
    cellel.text = "Text";
    cellel.ftsize = 18;
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
  }

  addInput(e) {
    var cell = this.scService.getCurrCellComp().cell;
    var cellel = new Cellel();
    cellel.type = "Input";
    cellel.value = "";
    cellel.height = 20;
    cellel.width = 50;
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    if(!this.scService.getCxForm()) {
      window.alert("Error!");
      return;
    }
    // console.log("form key = " + this.scService.getCxForm().$key);
    var inputNo = (this.scService.getCxForm().no_inputs+1);
    cellel.input_no = inputNo;
    this.scService.getCxForm().no_inputs = inputNo;
    
    this.toolbarService.updateCxform(this.scService.getCxForm());
    this.toolbarService.updatePage(this.scService.getCurrPage());

  }

  addTextArea(e) {
    var cell = this.scService.getCurrCellComp().cell;
    var cellel = new Cellel();
    cellel.type = "TextArea";
    cellel.text = "TextArea";
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }

  addHr(e) {
    var cell = this.scService.getCurrCellComp().cell;
    var cellel = new Cellel();
    cellel.type = "HR";
    cellel.width = (cell.width - 10)
    cellel.size = 5;
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }

  addImage(e) {
    let randomId = Math.random().toString(36).substring(2);
    // console.log("image ends in " + e.target.files[0].name);
    let width;
    let height;

    var fr = new FileReader;
    fr.onload = function () { // file is loaded
      var img = new Image;
      img.onload = function () {
        //            alert(img.width + "," + img.height); // image is loaded; sizes are available
        width = img.width;
        height = img.height;
      };
      img.src = fr.result.toString(); // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(e.target.files[0]); // I'm using a <input type="file"> for demonstrating

    let storageRef = firebase.storage().ref(randomId);
    storageRef.put(e.target.files[0]).then(any => {
      storageRef.getDownloadURL().then(url => {
        // console.log("cellel image 1");
        var cell = this.scService.getCurrCellComp().cell;
        var cellel = new Cellel();
        cellel.storageid = randomId;
        cellel.type = "Image";
        cellel.url = url;
        cellel.width = width;
        cellel.height = height;
        cellel.cellKey = cell.$key;
        cell.cellels.push(cellel);
        this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
        e.target.value = "";
      }).catch(function (error) {
        // console.log("error = " + (<Error>error).message);
      });
    });
  }

  addInnerTable(e) {
    var cell = this.scService.getCurrCellComp().cell;
    var cellel = new Cellel();
    cellel.type = "Table";
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    var table = new Table();
    table.cols.push(100);
    var row = new Row();
    row.row = 0;
    row.height = 25;
    table.rows.push(row);
    var cell = new Cell();
    cell.row = row.row;
    cell.col = 0;
    cell.colspan = 1;
    cell.rowspan = 1;
    cell.halign = "Center";
    cell.valign = "Middle";
    cell.width = 100;
    row.cells.push(cell);

    cellel.table = table;

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
   
  }
  delInnerTable(e) {

  }
  addInnerRow(e) {
    var row = new Row();
    var table = this.scService.getCurrITableComp().cellel.table;
    row.row = table.rows.length;
    row.height = 25;
    table.rows.push(row);

    for (var i = 0; i < table.cols.length; i++) {
      var cell = new Cell();
      cell.row = row.row;
      cell.col = row.cells.length;
      cell.colspan = 1;
      cell.rowspan = 1;
      cell.halign = "Center";
      cell.valign = "Middle";
      row.cells.push(cell);
    }
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }
  delInnerRow(e) {

  }
  addInnerCol(e) {
    var table = this.scService.getCurrITableComp().cellel.table;
    table.cols.push(100);
    for (var i = 0; i < table.rows.length; i++) {
      var cell = new Cell();
      cell.row = table.rows[i].row;
      cell.col = table.rows[i].cells.length;
      cell.colspan = 1;
      cell.rowspan = 1;
      cell.halign = "Center";
      cell.valign = "Middle";
      table.rows[i].cells.push(cell);
    }
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
  }

  delInnerCol(e) {
  }

  addIText(e) {
    var cell = this.scService.getCurrICell().cell;
    var cellel = new Cellel();
    cellel.type = "Text";
    cellel.text = "Text";
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
  }

  addIInput(e) {
    // window.alert("form key = " + this.scService.getCxForm().$key);
    var cell = this.scService.getCurrICell().cell;
    var cellel = new Cellel();
    cellel.type = "Input";
    cellel.value = "";
    cellel.height = 20;
    cellel.width = 50;
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    // console.log("form key = " + this.scService.getCxForm().$key);
    var inputNo = (this.scService.getCxForm().no_inputs+1);
    cellel.input_no = inputNo;
    this.scService.getCxForm().no_inputs = inputNo;
    
    this.toolbarService.updateCxform(this.scService.getCxForm());
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }

  addITextArea(e) {
    var cell = this.scService.getCurrICell().cell;
    var cellel = new Cellel();
    cellel.type = "TextArea";
    cellel.text = "TextArea";
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }

  addIHr(e) {
    var cell = this.scService.getCurrICell().cell;
    var cellel = new Cellel();
    cellel.type = "HR";
    cellel.width = (cell.width - 10)
    cellel.size = 5;
    cellel.cellKey = cell.$key;
    cell.cellels.push(cellel);

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);

  }

  addIImage(e) {
    let randomId = Math.random().toString(36).substring(2);
    // console.log("image ends in " + e.target.files[0].name);
    let width;
    let height;

    var fr = new FileReader;
    fr.onload = function () { // file is loaded
      var img = new Image;
      img.onload = function () {
        //            alert(img.width + "," + img.height); // image is loaded; sizes are available
        width = img.width;
        height = img.height;
      };
      img.src = fr.result.toString(); // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(e.target.files[0]); // I'm using a <input type="file"> for demonstrating

    let storageRef = firebase.storage().ref(randomId);
    storageRef.put(e.target.files[0]).then(any => {
      storageRef.getDownloadURL().then(url => {
        // console.log("cellel image 1");
        var cell = this.scService.getCurrICell().cell;
        var cellel = new Cellel();
        cellel.storageid = randomId;
        cellel.type = "Image";
        cellel.url = url;
        cellel.width = width;
        cellel.height = height;
        cellel.cellKey = cell.$key;
        cell.cellels.push(cellel);
        this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
        e.target.value = "";
      }).catch(function (error) {
        // console.log("error = " + (<Error>error).message);
      });
    });
  }

  addProject(e: Event) {
    var projId = this.afs.createId();
    var projRef = this.afs.firestore.collection('projects').doc(projId);
    var project = new Project();
    project.$key = projId;
    project.name = "New Project";
    project.account_key = this.scService.getCurrAccount().$key;
    projRef.set(JSON.parse(JSON.stringify(project)));
    return;
  }

  addAsset(e: Event) {
    let assetId = this.afs.createId();
    var assetRef = this.afs.firestore.collection('assets').doc(assetId);
    var asset = new Asset();
    asset.$key = assetId;
    asset.type = "Asset Type";
    asset.project_key = this.scService.getCurrProject().$key;
    assetRef.set(JSON.parse(JSON.stringify(asset)));
    return;
  }

  addForm(e: Event) {
    if (this.scService.getCurrAsset == null) {
      return;
    }
    let formId = this.afs.createId();
    var formRef = this.afs.firestore.collection('forms').doc(formId);
    var form = new Cxform();
    form.$key = formId;
    form.name = "New Form";
    form.asset_key = this.scService.getCurrAsset().$key;
    formRef.set(JSON.parse(JSON.stringify(form)));
    var page = new Page();
    page.formKey = form.$key;
    page.pageno = 1;
    this.afs.collection('pages').add(JSON.parse(JSON.stringify(page)));
    return;
  }

  gotoEditor(e: Event) {
    this.router.navigate(['/', 'mainwin']);
  }

  genReport(e: Event) {
    // console.log("genReport");
    if (this.scService.getCurrProject()) {
      // console.log("about to call genReport()!")
      // console.log(this.pdfUrl + this.scService.getCurrProject().$key);
      window.open(this.pdfUrl + this.scService.getCurrProject().$key);
    }
  }

  genrefReport(e: Event) {
    // console.log("genrefReport");
    if (this.scService.getCurrProject()) {
      // console.log("about to call genrefReport()!")
      // console.log(this.pdfrefUrl + this.scService.getCurrProject().$key);
      window.open(this.pdfrefUrl + this.scService.getCurrProject().$key);
    }
  }

  toggleformView(e: Event) {
    // console.log("toggleformView");
    this.scService.setCurrMode("form");
  }

  toggleformrefView(e: Event) {
    // console.log("toggleformrefView");
    this.scService.setCurrMode("formref");
  }

  toggleassetView(e: Event) {
    // console.log("toggleassetView");
    this.scService.setCurrMode("asset");
  }

  ngOnInit() {
    // console.log("ngOnInit window.location = " + window.location);
    if (window.location.hostname.includes("localhost")) {
      this.pdfUrl = this.local_pdfUrl;
      this.pdfrefUrl = this.local_pdfrefUrl;
    } else {
      this.pdfUrl = this.serve_pdfUrl;
      this.pdfrefUrl = this.serve_pdfrefUrl;
    }

  }

  delElement(e) {
  }


  redo(e) {
    this.doundoService.redo();
  }
  undo(e) {
    // console.log("Here!");
    this.doundoService.commandSize();
    this.doundoService.undo();
  }

  pdfIt(e) {
    //  const doc = new PDFDocument();

  }

  gotoOps(e: Event) {
    this.router.navigate(['/']);
  }

  logOut(e: Event) {
    this.afAuth.auth.signOut();
  }
  
}