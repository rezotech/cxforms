import { Component, Input } from '@angular/core';
import { Cxform } from '../cxforms/shared/cxform.model';
import { Router } from '@angular/router';
import { ScService } from '../sc.service';

@Component({
  selector: 'sidebarcomponent',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent {
  @Input() cxform: Cxform;
  local_pdfUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/genPdf";
  serve_pdfUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/genPdf";
  pdfUrl: string = this.serve_pdfUrl;
  hover: boolean;
  
  constructor(private scService: ScService) {
  }

  setCxform() {
    console.log("Form = " + this.cxform.name);
    this.scService.setCxform(this.cxform);
  }

  ngOnInit() {
  }

}