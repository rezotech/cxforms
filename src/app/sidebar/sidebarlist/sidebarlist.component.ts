import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ToolbarService } from '../../toolbar/toolbar.service';
import { Cxform } from '../../cxforms/shared/cxform.model';

@Component({
  selector: 'app-sidebarlist',
  templateUrl: './sidebarlist.component.html',
  styleUrls: ['./sidebarlist.component.css']
})

export class SidebarlistComponent implements OnInit {
  forms: Observable<Cxform[]>;
  toolbarService: ToolbarService;
 
  constructor(@Inject(ToolbarService) toolbarService) {
    this.toolbarService = toolbarService;
    this.forms = this.toolbarService.getCxforms();
    
  }

  ngOnInit() {
  }

}