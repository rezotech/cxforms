import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ScService } from '../../../../sc.service';
import { Observable } from 'rxjs';
import { Cxform } from '../../../../cxforms/shared/cxform.model';
import { Assetref } from '../../../../cxforms/shared/assetref.model';
import { Formref } from '../../../../cxforms/shared/formref.model';

@Component({
  selector: 'app-ereflistitemform',
  templateUrl: './ereflistitemform.component.html',
  styleUrls: ['./ereflistitemform.component.css']
})
export class EreflistitemformComponent implements OnInit {
  @Input() assetref: Assetref;
  formrefs: Observable<Cxform[]>;
  local_pdfUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/pdfRef";
  serve_pdfUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/pdfRef";
  pdfUrl: string = this.local_pdfUrl;

  constructor(private afs: AngularFirestore, private scService: ScService) {
    // this.scService.assetment_messageSource.subscribe(asset => {
    //   if(asset==null) {
    //     return;
    //   }
    //   console.log("new asset! key = " + asset.$key);
    //   this.forms = this.afs.collection<Cxform>('forms', ref => ref.where('asset_key', "==", asset.$key)).valueChanges();
    //   this.forms.subscribe(_forms => {
    //     console.log("subscribe()");
    //     _forms.forEach(_form => {
    //       console.log("form key = " + _form.$key);
    //     });
    //   });
    // });

  }

  onClick(e: Event, formref: Formref) {
    this.scService.setCurrFormref(formref);
    this.scService.setCurrMode("formref");
    console.log("window.location = " + window.location);

  }

  ngOnInit() {
    this.formrefs = this.afs.collection<Cxform>('formrefs', ref => ref.where('assetref_key', "==", this.assetref.$key)).valueChanges();
    if(window.location.hostname.includes("localhost")) {
      this.pdfUrl = this.local_pdfUrl;
    } else {
      this.pdfUrl = this.serve_pdfUrl;
    }
    // this.forms.subscribe(_forms => {
    //   console.log("subscribe()");
    //   _forms.forEach(_form => {
    //     console.log("form key = " + _form.$key);
    //   });
    // });
  }
}
