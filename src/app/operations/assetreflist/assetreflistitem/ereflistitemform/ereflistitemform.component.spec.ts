import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EreflistitemformComponent } from './ereflistitemform.component';

describe('EreflistitemformComponent', () => {
  let component: EreflistitemformComponent;
  let fixture: ComponentFixture<EreflistitemformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EreflistitemformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EreflistitemformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
