import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetreflistitemComponent } from './assetreflistitem.component';

describe('AssetreflistitemComponent', () => {
  let component: AssetreflistitemComponent;
  let fixture: ComponentFixture<AssetreflistitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetreflistitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetreflistitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
