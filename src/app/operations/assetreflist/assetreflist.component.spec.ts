import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetreflistComponent } from './assetreflist.component';

describe('AssetreflistComponent', () => {
  let component: AssetreflistComponent;
  let fixture: ComponentFixture<AssetreflistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetreflistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetreflistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
