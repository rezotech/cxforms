import { Component, OnInit } from '@angular/core';
import { Asset } from '../cxforms/shared/asset.model';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { Project, LevelItem } from '../cxforms/shared/project.model';
import { ScService } from '../sc.service';
import { Cxform } from '../cxforms/shared/cxform.model';
import { Page } from '../cxforms/shared/page.model';


@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})

export class OperationsComponent implements OnInit {
  assetId: string = "assetId";

  local_pdfUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/genReport?projectKey=";
  serve_pdfUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/genReport?projectKey=";
  pdfUrl: string = this.local_pdfUrl;

  constructor(
    private afs: AngularFirestore,
    private scService: ScService,
    private router: Router
  ) {
    this.scService.assetment_messageSource.subscribe(asset => {
      if(asset==null) {
        return;
      }
      this.assetId = asset.$key;
    });
  }

  addProject(e: Event) {
    var projId = this.afs.createId();
    var projRef = this.afs.firestore.collection('projects').doc(projId);
    var project = new Project();
    project.$key = projId;
    project.name = "New Project";
    project.account_key = this.scService.getCurrAccount().$key;
    projRef.set(JSON.parse(JSON.stringify(project)));
    return;
  }

  addAsset(e: Event) {
    let assetId = this.afs.createId();
    var assetRef = this.afs.firestore.collection('assets').doc(assetId);
    var asset = new Asset();
    asset.$key = assetId;
    asset.type = "Asset Type";
    asset.project_key = this.scService.getCurrProject().$key;
    assetRef.set(JSON.parse(JSON.stringify(asset)));
    return;
  }

  addForm(e: Event) {
    if(this.scService.getCurrAsset==null) {
      return;
    }
    let formId = this.afs.createId();
    var formRef = this.afs.firestore.collection('forms').doc(formId);
    var form = new Cxform();
    form.$key = formId;
    form.name = "New Form";
    form.asset_key = this.scService.getCurrAsset().$key;
    formRef.set(JSON.parse(JSON.stringify(form)));
    var page = new Page();
    page.formKey = form.$key;
    page.pageno = 1;
    this.afs.collection('pages').add(JSON.parse(JSON.stringify(page)));
    return;
  }

  gotoEditor(e: Event) {
    this.router.navigate(['/', 'mainwin']);
  }

  genReport(e: Event) {
    console.log("genReport");
    if(this.scService.getCurrProject()) {
      console.log("about to call genReport()!")
      console.log(this.pdfUrl + this.scService.getCurrProject().$key);
      window.open(this.pdfUrl + this.scService.getCurrProject().$key);
    }
  }

   ngOnInit() {
    console.log("ngOnInit window.location = " + window.location);
    if(window.location.hostname.includes("localhost")) {
      this.pdfUrl = this.local_pdfUrl;
    } else {
      this.pdfUrl = this.serve_pdfUrl;
    }
    
  }

}
