import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Asset } from '../../cxforms/shared/asset.model';
import { Observable } from 'rxjs';
import { ScService } from '../../sc.service';
import { Project } from '../../cxforms/shared/project.model';
import { Assetref } from '../../cxforms/shared/assetref.model';
import { Cxform } from '../../cxforms/shared/cxform.model';
import { Page } from '../../cxforms/shared/page.model';
import { Formref } from '../../cxforms/shared/formref.model';

@Component({
  selector: 'app-assetlist',
  templateUrl: './assetlist.component.html',
  styleUrls: ['./assetlist.component.css']
})

export class AssetlistComponent implements OnInit {
  assetCollection: AngularFirestoreCollection<Asset>;
  assets: Observable<Asset[]>;
  assetrefs: Observable<Assetref[]>;

  constructor(
    private afs: AngularFirestore,
    private scService: ScService
  ) {
    this.scService.project_messageSource.subscribe(project => {
      if (project == null) {
        // console.log("project == null!");
        return;
      }
      this.assets = this.afs.collection<Asset>('assets', ref => ref.where('project_key', "==", project.$key)).valueChanges();
      this.assetrefs = this.afs.collection<Assetref>('assetrefs', ref => ref.where('project_key', "==", project.$key)).valueChanges();

    });

  }

  async addAsset(e: Event) {
    // console.log("addAsset(e: Event)");
    if (this.scService.getAssetList().length > 0) {
      // console.log("Good to go!");
      if (this.scService.getCurrProject() == null) {
        return;
      }
      var assets = await this.scService.getAssetList();
      for (var i = 0; i < assets.length; i++) {
        // console.log("1");
        var asset = assets[i];
        let assetRefId = await this.afs.createId();

        var forms = await this.afs.firestore.collection('forms').where('asset_key', '==', asset.$key);
        var batch = this.afs.firestore.batch();

        await forms.get().then(async (p) => {
          for (let j = 0; j < p.docs.length; j++) {
            // console.log("2");
            var form = p.docs[j].data() as Cxform;
            var formKey = form.$key;
            form.asset_key = assetRefId;
            var formRefId = await this.afs.createId();
            var formRef = await this.afs.firestore.collection('forms').doc(formRefId);
            form.$key = formRefId;
            batch.set(formRef, JSON.parse(JSON.stringify(form)));
            // formRef.set(JSON.parse(JSON.stringify(form)));
            // console.log("Add Form!");
            var pages = await this.afs.firestore.collection('pages').where('formKey', '==', formKey);
            await pages.get().then(async (q) => {
              for (let k = 0; k < q.docs.length; k++) {
                // console.log("3");
                var page = q.docs[k].data() as Page;
                page.formKey = form.$key;
                var pageRefId = await this.afs.createId();
                var pageRef = await this.afs.firestore.collection('pages').doc(pageRefId);
                page.$key = pageRefId;
                batch.set(pageRef, JSON.parse(JSON.stringify(page)));
                // await pageRef.set(JSON.parse(JSON.stringify(page)));
                // console.log("Add Page!");
              }
            });
          }
        }).catch(e => {
          // console.log(e);
        });

        asset.project_key = this.scService.getCurrProject().$key;
        var assetRef = await this.afs.firestore.collection('assets').doc(assetRefId);
        asset.$key = assetRefId;
        batch.set(assetRef, JSON.parse(JSON.stringify(asset)));
        // assetRef.set(JSON.parse(JSON.stringify(asset)));
        
        // console.log("4");
        batch.commit().then(function() {
          // console.log("Asset Added!");
        });

      }
      this.scService.clearAssetList();
    } else {
      // console.log("Not Good to go!");
    }

  }

  async addAssetref(e: Event) {
    // console.log("addAssetref(e: Event)");
    if (this.scService.getAssetList().length > 0) {
      // console.log("Good to go!");
      if (this.scService.getCurrProject() == null) {
        return;
      }
      var assets = this.scService.getAssetList();
      for (var i = 0; i < assets.length; i++) {
        var asset = assets[i];

        var _assetref = new Assetref();
        _assetref.project_key = this.scService.getCurrProject().$key;
        _assetref.asset_key = asset.$key;
        _assetref.level1 = asset.level1;
        _assetref.level2 = asset.level2;
        _assetref.level3 = asset.level3;
        _assetref.level4 = asset.level4;
        _assetref.level5 = asset.level5;
        _assetref.make = asset.make;
        _assetref.model = asset.model;
        _assetref.name = asset.name;

        var batch = this.afs.firestore.batch();
        let assetRefId = this.afs.createId();
        var assetRef = this.afs.firestore.collection('assetrefs').doc(assetRefId);
        _assetref.$key = assetRefId;
        batch.set(assetRef, JSON.parse(JSON.stringify(_assetref)));
        // assetRef.set(JSON.parse(JSON.stringify(_assetref)));
        // console.log("Add Assetref!");

        var forms = await this.afs.firestore.collection('forms').where('asset_key', '==', asset.$key);

        await forms.get().then(async (p) => {
          for (let j = 0; j < p.docs.length; j++) {
            // console.log("2");
            var form = p.docs[j].data() as Cxform;
            var formKey = form.$key;
            var formref = new Formref();
            formref.form_key = formKey;
            formref.assetref_key = _assetref.$key;
            var formRefId = await this.afs.createId();
            var _formref = await this.afs.firestore.collection('formrefs').doc(formRefId);
            formref.$key = formRefId;
            batch.set(_formref, JSON.parse(JSON.stringify(formref)));
            // formRef.set(JSON.parse(JSON.stringify(form)));
            // console.log("Add Formref!");
          }

          batch.commit().then(function() {
            // console.log("Assetref Added!");
          });
        }).catch(e => {
          // console.log(e);
        });

      }
      this.scService.clearAssetList();
    } else {
      // console.log("Not Good to go!");
    }

  }

  ngOnInit() {
    // console.log("ngOnInit() #1");



  }

}
