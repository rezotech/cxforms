import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Asset } from '../../../cxforms/shared/asset.model';
import { ScService } from '../../../sc.service';
import { Cxform } from '../../../cxforms/shared/cxform.model';
import { Observable } from 'rxjs';
import { ToolbarService } from '../../../toolbar/toolbar.service';

@Component({
  selector: '[app-assetlistitem]',
  templateUrl: './assetlistitem.component.html',
  styleUrls: ['./assetlistitem.component.css']
})
export class AssetlistitemComponent implements OnInit {
  @Input() asset: Asset;
  @ViewChild("cb0", { read: ElementRef }) cb0Div: ElementRef;
  @ViewChild("tdiv0", { read: ElementRef }) text0Div: ElementRef;
  @ViewChild("idiv0", { read: ElementRef }) input0Div: ElementRef;
  @ViewChild("tdiv1", { read: ElementRef }) text1Div: ElementRef;
  @ViewChild("idiv1", { read: ElementRef }) input1Div: ElementRef;
  @ViewChild("tdiv2", { read: ElementRef }) text2Div: ElementRef;
  @ViewChild("idiv2", { read: ElementRef }) input2Div: ElementRef;
  @ViewChild("tdiv3", { read: ElementRef }) text3Div: ElementRef;
  @ViewChild("idiv3", { read: ElementRef }) input3Div: ElementRef;
  @ViewChild("tdiv4", { read: ElementRef }) text4Div: ElementRef;
  @ViewChild("idiv4", { read: ElementRef }) input4Div: ElementRef;
  @ViewChild("tdiv5", { read: ElementRef }) text5Div: ElementRef;
  @ViewChild("idiv5", { read: ElementRef }) input5Div: ElementRef;
  hover: boolean;

  constructor(private scService: ScService, private toolbarService: ToolbarService) {
   
  }
  toggleInputLevel0(e: Event) {
    console.log("Input value = " + this.input0Div.nativeElement.value);
    this.text0Div.nativeElement.style.display = "block";
    this.input0Div.nativeElement.style.display = "none";
    this.asset.name = this.input0Div.nativeElement.value.trim();
    this.toolbarService.updateAsset(this.asset);
  }
  toggleTxtLevel0(e: Event) {
    console.log("text = " + this.text0Div.nativeElement.textContent.trim());
    this.text0Div.nativeElement.style.display = "none";
    this.input0Div.nativeElement.style.display = "block";
    this.input0Div.nativeElement.value = this.text0Div.nativeElement.textContent.trim();
    this.input0Div.nativeElement.focus();
  }
  toggleInputLevel1(e: Event) {
    this.text1Div.nativeElement.style.display = "block";
    this.input1Div.nativeElement.style.display = "none";

    var ival: string = (this.input1Div.nativeElement.value).trim();
    console.log("Input value = " + ival);
    if(ival.length==0) {
      this.asset.level1 = null;
    } else {
      this.asset.level1 = this.input1Div.nativeElement.value.trim();
    }

    this.toolbarService.updateAsset(this.asset);
  }
  toggleTxtLevel1(e: Event) {
    console.log("text = " + this.text1Div.nativeElement.textContent.trim());
    this.text1Div.nativeElement.style.display = "none";
    this.input1Div.nativeElement.style.display = "block";
    this.input1Div.nativeElement.value = this.text1Div.nativeElement.textContent.trim();
    this.input1Div.nativeElement.focus();
  }
  toggleInputLevel2(e: Event) {
    console.log("Input value = " + this.input2Div.nativeElement.value);
    this.text2Div.nativeElement.style.display = "block";
    this.input2Div.nativeElement.style.display = "none";

    var ival: string = (this.input2Div.nativeElement.value).trim();
    console.log("Input value = " + ival);
    if(ival.length==0) {
      this.asset.level2 = null;
    } else {
      this.asset.level2 = this.input2Div.nativeElement.value.trim();
    }
    this.toolbarService.updateAsset(this.asset);
  }
  toggleTxtLevel2(e: Event) {
    console.log("text = " + this.text2Div.nativeElement.textContent.trim());
    this.text2Div.nativeElement.style.display = "none";
    this.input2Div.nativeElement.style.display = "block";
    this.input2Div.nativeElement.value = this.text2Div.nativeElement.textContent.trim();
    this.input2Div.nativeElement.focus();
  }
  toggleInputLevel3(e: Event) {
    console.log("Input value = " + this.input3Div.nativeElement.value);
    this.text3Div.nativeElement.style.display = "block";
    this.input3Div.nativeElement.style.display = "none";

    var ival: string = (this.input3Div.nativeElement.value).trim();
    console.log("Input value = " + ival);
    if(ival.length==0) {
      this.asset.level3 = null;
    } else {
      this.asset.level3 = this.input3Div.nativeElement.value.trim();
    }
    this.toolbarService.updateAsset(this.asset);
  }
  toggleTxtLevel3(e: Event) {
    console.log("text = " + this.text3Div.nativeElement.textContent.trim());
    this.text3Div.nativeElement.style.display = "none";
    this.input3Div.nativeElement.style.display = "block";
    this.input3Div.nativeElement.value = this.text3Div.nativeElement.textContent.trim();
    this.input3Div.nativeElement.focus();
  }
  toggleInputLevel4(e: Event) {
    console.log("Input value = " + this.input4Div.nativeElement.value);
    this.text4Div.nativeElement.style.display = "block";
    this.input4Div.nativeElement.style.display = "none";

    var ival: string = (this.input4Div.nativeElement.value).trim();
    console.log("Input value = " + ival);
    if(ival.length==0) {
      this.asset.level4 = null;
    } else {
      this.asset.level4 = this.input4Div.nativeElement.value.trim();
    }
    this.toolbarService.updateAsset(this.asset);
  }
  toggleTxtLevel4(e: Event) {
    console.log("text = " + this.text4Div.nativeElement.textContent.trim());
    this.text4Div.nativeElement.style.display = "none";
    this.input4Div.nativeElement.style.display = "block";
    this.input4Div.nativeElement.value = this.text4Div.nativeElement.textContent.trim();
    this.input4Div.nativeElement.focus();
  }
  toggleInputLevel5(e: Event) {
    console.log("Input value = " + this.input5Div.nativeElement.value);
    this.text5Div.nativeElement.style.display = "block";
    this.input5Div.nativeElement.style.display = "none";

    var ival: string = (this.input5Div.nativeElement.value).trim();
    console.log("Input value = " + ival);
    if(ival.length==0) {
      this.asset.level5 = null;
    } else {
      this.asset.level5 = this.input5Div.nativeElement.value.trim();
    }
    this.toolbarService.updateAsset(this.asset);
  }
  toggleTxtLevel5(e: Event) {
    console.log("text = " + this.text5Div.nativeElement.textContent.trim());
    this.text5Div.nativeElement.style.display = "none";
    this.input5Div.nativeElement.style.display = "block";
    this.input5Div.nativeElement.value = this.text5Div.nativeElement.textContent.trim();
    this.input5Div.nativeElement.focus();
  }
  checkIt(e: Event) {
    console.log("check = " + this.cb0Div.nativeElement.checked);;
    if(this.cb0Div.nativeElement.checked) {
      this.scService.addToAssetList(this.asset);
    } else {
      this.scService.removeFromAssetList(this.asset);
    }

  }

  setAsset(e: Event) {
    console.log("setAsset");
    this.scService.setCurrAsset(this.asset);

  }

  ngOnInit() {
  }

}
