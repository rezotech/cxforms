import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ScService } from '../../../../sc.service';
import { Observable } from 'rxjs';
import { Cxform } from '../../../../cxforms/shared/cxform.model';
import { Asset } from '../../../../cxforms/shared/asset.model';

@Component({
  selector: 'app-elistitemform',
  templateUrl: './elistitemform.component.html',
  styleUrls: ['./elistitemform.component.css']
})
export class ElistitemformComponent implements OnInit {
  @Input() asset: Asset;
  forms: Observable<Cxform[]>;

  constructor(private afs: AngularFirestore, private scService: ScService) {
    // this.scService.assetment_messageSource.subscribe(asset => {
    //   if(asset==null) {
    //     return;
    //   }
    //   console.log("new asset! key = " + asset.$key);
    //   this.forms = this.afs.collection<Cxform>('forms', ref => ref.where('asset_key', "==", asset.$key)).valueChanges();
    //   this.forms.subscribe(_forms => {
    //     console.log("subscribe()");
    //     _forms.forEach(_form => {
    //       console.log("form key = " + _form.$key);
    //     });
    //   });
    // });

  }

  ngOnInit() {
    this.forms = this.afs.collection<Cxform>('forms', ref => ref.where('asset_key', "==", this.asset.$key)).valueChanges();
    // this.forms.subscribe(_forms => {
    //   console.log("subscribe()");
    //   _forms.forEach(_form => {
    //     console.log("form key = " + _form.$key);
    //   });
    // });
  }
}
