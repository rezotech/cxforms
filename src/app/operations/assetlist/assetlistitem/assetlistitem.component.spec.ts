import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetlistitemComponent } from './assetlistitem.component';

describe('AssetlistitemComponent', () => {
  let component: AssetlistitemComponent;
  let fixture: ComponentFixture<AssetlistitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetlistitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetlistitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
