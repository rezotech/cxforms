import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { ScService } from './sc.service';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { Account } from './cxforms/shared/account.model';
import { Project } from './cxforms/shared/project.model';
import { User } from './cxforms/shared/user.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild("namein", { read: ElementRef }) nameEl: ElementRef;
  @ViewChild("pwin", { read: ElementRef }) pwEl: ElementRef;
  title = 'app';

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private scService: ScService,
    private router: Router,
  ) {
    // afs.firestore.enablePersistence()
    //   .then(function () {
    //     var db = firebase.firestore();
    //   })
    //   .catch(function (err) {
    //     if (err.code == 'failed-precondition') {
    //       // Multiple tabs open, persistence can only be enabled
    //       // in one tab at a a time.
    //       // ...
    //     } else if (err.code == 'unimplemented') {
    //       // The current browser does not support all of the
    //       // features required to enable persistence
    //       // ...
    //     }
    //   });
    // console.log("Device Info = " + deviceInfo.device);
    // scService.setDeviceType(deviceInfo.device);
    // if (deviceInfo.device != "unknown") {
    //   // console.log("Android!");
    //   this.router.navigate(['/mobilemain']);
    // } else {
    //   // console.log("Desktop!");
    // }
    this.afAuth.auth.onAuthStateChanged((auth) => {
      if (auth != null) {
        // console.log("auth.email = " + auth.email);
        var user: Observable<User>;
        user = this.afs.doc<User>('users/' + auth.email).valueChanges();
        user.subscribe(_user => {
          this.scService.setCurrUser(_user);
        });

        var account: Observable<Account[]>;
        account = this.afs.collection<Account>('accounts', ref => ref.where('user_key', '==', auth.email)).valueChanges();
        account.subscribe(_accounts => {
          _accounts.forEach(_account => {
            this.scService.setCurrAccount(_account);
            // console.log("Account = " + _account.user_key);
          });
        });

      } else {
        // console.log("auth == null!");
      }
    });
    this.scService.cxForm_messageSource.subscribe((cxForm => {
      if (cxForm != null) {
        // console.log("cxForm = " + cxForm.name);
        this.title = cxForm.$key;
      }
    }));
  }

  login() {
    window.alert("login()! email = " + this.nameEl.nativeElement.value + ", pw = " + this.pwEl.nativeElement.value);
    this.afAuth.auth.signInWithEmailAndPassword(this.nameEl.nativeElement.value, this.pwEl.nativeElement.value)
      .then(value => {
        window.alert(value.email);
        // console.log('Nice, it worked!');
        this.router.navigate(['/mainwin']);
        // if (this.scService.getDeviceType() != "unknown") {
        //   this.router.navigate(['/mobilemain']);
        // } else {
        //   this.router.navigate(['/mainwin']);
        // }

      })
      .catch(err => {
        console.log('Something went wrong: ', err.message);
      });
  }

  validEmail(email: string): boolean {
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (email != "" && (email.length <= 5 || !EMAIL_REGEXP.test(email))) {
      return false;
    }
    return true;
  }

  create() {
    window.alert("create()! email = " + this.nameEl.nativeElement.value + ", pw = " + this.pwEl.nativeElement.value);
    if (!this.validEmail(this.nameEl.nativeElement.value)) {
      window.alert("Invalid Email!");
      return;
    }
    firebase.auth().createUserWithEmailAndPassword(this.nameEl.nativeElement.value, this.pwEl.nativeElement.value).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("errorCode = " + error.code);
      console.log("errorMessage = " + error.message);
    }).then((user) => {
      var email: string = user.email;
      window.alert("email = " + email);
      var batch = this.afs.firestore.batch();

      var userRef = this.afs.firestore.collection('users').doc(email);
      var userDb: User = new User();
      userDb.$key = email;
      userDb.email = email;
      userDb.name = "David";
      batch.set(userRef, JSON.parse(JSON.stringify(userDb)));

      let acctId = this.afs.createId();
      var acntRef = this.afs.firestore.collection('accounts').doc(acctId);
      var account = new Account();
      account.$key = acctId;
      account.user_key = userDb.$key;
      batch.set(acntRef, JSON.parse(JSON.stringify(account)));

      let projId = this.afs.createId();
      var projRef = this.afs.firestore.collection('projects').doc(projId);
      var project = new Project();
      project.$key = projId;
      project.name = "My Project";
      project.account_key = account.$key;
      batch.set(projRef, JSON.parse(JSON.stringify(project)));

      batch.commit().then(function () {
        window.alert("Account Created!");
      });

    });
  }

  logout() {
    this.afAuth.auth.signOut();
  }

}
