import { Injectable, ElementRef } from '@angular/core';

import { SidebarComponent } from './sidebar/sidebar.component';
import { CxformComponent } from './cxforms/cxform/cxform.component';
import { PageComponent } from './cxforms/cxform/page/page.component';
import { TableComponent } from './cxforms/cxform/table/table.component';
import { RowComponent } from './cxforms/cxform/table/row/row.component';
import { CellComponent } from './cxforms/cxform/table/cell/cell.component';
import { Cxform } from './cxforms/shared/cxform.model';
import { Assetref } from './cxforms/shared/assetref.model';
import { Formref } from './cxforms/shared/formref.model';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Cell } from './cxforms/shared/cell.model';
import { Cellel } from './cxforms/shared/cellel.model';
import { TxtelComponent } from './cxforms/cxform/table/cellel/txtel/txtel.component';
import { Row } from './cxforms/shared/row.model';
import { User } from './cxforms/shared/user.model';
import { Account } from './cxforms/shared/account.model';
import { Project } from './cxforms/shared/project.model';
import { Asset } from './cxforms/shared/asset.model';
import { InpelComponent } from './cxforms/cxform/table/cellel/inpel/inpel.component';
import { MainwinComponent } from './mainwin/mainwin.component';
import { ItelComponent } from './cxforms/cxform/table/cellel/itel/itel.component';
import { Page } from './cxforms/shared/page.model';
import { Table } from './cxforms/shared/table.model';
import { MobilecxformComponent } from './mobile/mobilecxform/mobilecxform.component';

@Injectable()
export class ScService {

    currUser: User;
    currAccount: Account;
    currProjects: Project[] = [];
    currProject: Project;
    currAssets: Asset[] = [];
    currAsset: Asset;
    currAssetref: Assetref;
    currCxformComp: CxformComponent;
    cxForm: Cxform;
    currFormref: Formref;
    formsMode: string = "form";
    currPageComp: PageComponent;
    currPage: Page;
    currTableComp: TableComponent;
    currITableComp: ItelComponent;
    currRow: Row;
    currCellComp: CellComponent;
    curriCellComp: CellComponent;
    currCellEl: Cellel;
    deviceType: string;

    cellRange: CellComponent[] = [];
    icellRange: CellComponent[] = [];

    clipPageEl: Page;
    clipTableEl: Table;
    clipCellEls: Cellel[] = [];

    colspan: number;
    rowspan: number;
    icolspan: number;
    irowspan: number;

    imerge: boolean = false;
    mousedn: boolean = false;

    mobileFormComp: MobilecxformComponent;

    user_messageSource = new BehaviorSubject<User>(null);
    account_messageSource = new BehaviorSubject<Account>(null);
    project_messageSource = new BehaviorSubject<Project>(null);
    assetment_messageSource = new BehaviorSubject<Asset>(null);
    assetmentref_messageSource = new BehaviorSubject<Assetref>(null);
    cxForm_messageSource = new BehaviorSubject<Cxform>(null);
    formRef_messageSource = new BehaviorSubject<Formref>(null);
    page_messageSource = new BehaviorSubject<PageComponent>(null);
    table_messageSource = new BehaviorSubject<TableComponent>(null);
    itable_messageSource = new BehaviorSubject<ItelComponent>(null);
    row_messageSource = new BehaviorSubject<Row>(null);
    cell_messageSource = new BehaviorSubject<Cell>(null);
    cellel_messageSource = new BehaviorSubject<Cellel>(null);
    mode_messageSource = new BehaviorSubject(<string>("form"));

    setCurrCellEl(currCellEl: Cellel): any {
        this.currCellEl = currCellEl;
        this.cellel_messageSource.next(this.currCellEl);
    }

    getCurrCellEl(): Cellel {
        return this.currCellEl;
    }

    addToAssetList(asset: Asset) {
        this.currAssets.push(asset);
        // console.log("add assets length = " + this.currAssets.length);
    }

    removeFromAssetList(asset: Asset) {
        this.currAssets.splice(this.currAssets.indexOf(asset), 1);
        // console.log("remove assets length = " + this.currAssets.length);
    }

    clearAssetList() {
        this.currAssets = [];
    }

    getAssetList() {
        return this.currAssets;
    }


    setCurrMode(mode: string) {
        this.formsMode = mode;
        this.mode_messageSource.next(this.formsMode);
    }

    getCurrMode() {
        return this.formsMode;
    }

    setCurrUser(user: User) {
        if (user == null) {
            return;
        }
        this.currUser = user;
        this.user_messageSource.next(user);
        // console.log("setCurrUser() = " + user.$key);
    }

    getCurrUser() {
        return this.currUser;
    }

    setCurrAccount(account: Account) {
        this.currAccount = account;
        this.account_messageSource.next(account);
        // console.log("setCurrAccount() = " + account.$key);
    }

    getCurrAccount() {
        return this.currAccount;
    }

    setCurrProject(project: Project) {
        if (project == null) {
            return;
        }
        this.currProject = project;
        this.project_messageSource.next(project);
        // console.log("setCurrProject() = " + project.$key);
    }

    getCurrProject() {
        return this.currProject;
    }

    setCurrAsset(asset: Asset) {
        this.currAsset = asset;
        this.assetment_messageSource.next(asset);
        // console.log("setCurrAsset() = " + asset.$key);
    }

    getCurrAsset() {
        return this.currAsset;
    }

    setCurrAssetref(assetref: Assetref) {
        this.currAssetref = assetref;
        this.assetmentref_messageSource.next(assetref);
        // console.log("setCurrAssetref() = " + assetref.$key);
    }

    getCurrAssetref() {
        return this.currAssetref;
    }

    setCurrCxformComp(cxFormComp: CxformComponent) {
        this.currCxformComp = cxFormComp;
    }

    getCurrCxformComp() {
        return this.currCxformComp;
    }
    
    setCxform(form) {
        this.cxForm = form;
        this.cxForm_messageSource.next(this.cxForm);
        // if(!form) {
        //     window.alert("null!");
        // }
        // window.alert("formkey = " + this.cxForm.$key);
    }

    getCxForm() {
        return this.cxForm;
    }
    setCurrFormref(formref) {
        this.currFormref = formref;
        this.formRef_messageSource.next(this.currFormref);
    }
    getCurrFormref() {
        return this.currFormref;
    }
    setCurrPageComp(pageComp: PageComponent) {
        // console.log("setCurrPageComp()");
        // if(this.currPageComp && pageComp.page.$key==this.currPageComp.page.$key) {
        //     console.log("return!");
        //     return;
        // }
        // console.log("not return!");
        this.currPageComp = pageComp;
        this.setCurrPage(pageComp.page);
        console.log("setCurrPage Page = " + this.currPageComp.page.pageno);
        console.log("key = " + this.currPageComp.page.formKey);
        console.log("tables = " + this.currPageComp.page.tables.length);
        this.page_messageSource.next(this.currPageComp);
    }
    getCurrPageComp() {
        return this.currPageComp;
    }
    setCurrPage(page: Page) {
        console.log("setCurrPage()! key = " + page.$key);
        this.currPage = page;
    }
    getCurrPage() {
        console.log("No tables = " + this.currPage.tables.length);
        return this.currPage;
    }
    setCurrTableComp(tableComp) {
        this.currTableComp = tableComp;
        this.table_messageSource.next(this.currTableComp);
        this.currCxformComp.attachChanges();
    }
    getCurrTableComp() {
        return this.currTableComp;
    }
    setCurrITableComp(itelComp) {
        this.currITableComp = itelComp;
        this.itable_messageSource.next(this.currITableComp);
    }
    getCurrITableComp() {
        return this.currITableComp;
    }
    setCurrRow(row) {
        this.currRow = row;
        this.row_messageSource.next(this.currRow);
        // console.log("setCurrRow Row key = " + this.currRow.row.$key);
    }
    getCurrRow() {
        return this.currRow;
    }
    setCurrCellComp(cellComp) {
        this.currCellComp = cellComp;
        // this.cellRange = [];
        // this.cellRange.push(cell);
        //        console.log("setCurrCell Cell key = " + this.currCell.cell.$key);
    }

    getCurrCellComp() {
        return this.currCellComp;
    }
    setCurrCell(td: HTMLElement, cell: Cell) {
        let cellComp: CellComponent = this.currTableComp.getCellComp(cell);

        for (var i = 0; i < this.cellRange.length; i++) {
            this.cellRange[i].unhighlight();
        }
        this.cellRange.length = 0;

        if (this.currCellComp) {
            this.currCellComp.unhighlight();
        }
        this.currCellComp = cellComp;
        this.currCellComp.highlight();
        this.cellRange.push(this.currCellComp);
        this.cell_messageSource.next(cellComp.cell);
    }

    setCurrICell(td: HTMLElement, cell: Cell) {
        if (!this.currITableComp) {
            return;
        }
        let icellComp: CellComponent = this.currITableComp.getCellComp(cell);

        for (var i = 0; i < this.icellRange.length; i++) {
            this.icellRange[i].unhighlight();
        }
        this.icellRange.length = 0;

        if (this.curriCellComp) {
            this.curriCellComp.unhighlight();
        }
        this.curriCellComp = icellComp;
        this.curriCellComp.highlight();
        this.icellRange.push(this.curriCellComp);
    }

    getCurrICell() {
        return this.curriCellComp;
    }
    getClipCellEls() {
        return this.clipCellEls;
    }
    setClipCellEls(cellEls: Cellel[]) {
        this.clipCellEls = cellEls;
    }
    getClipPageEl() {
        return this.clipPageEl;
    }
    setClipPageEl(page: Page) {
        this.clipPageEl = page;
    }
    getClipTableEl() {
        return this.clipTableEl;
    }
    setClipTableEl(table: Table) {
        this.clipTableEl = table;
    }
    mouseDown(down: boolean) {
        this.mousedn = down;
    }
    getRowSpan(): number {
        return this.rowspan;
    }
    setRowSpan(rowspan: number) {
        this.rowspan = rowspan;
    }
    getColSpan(): number {
        return this.colspan;
    }
    setColSpan(colspan: number) {
        this.colspan = colspan;
    }
    getiRowSpan(): number {
        return this.irowspan;
    }
    setiRowSpan(irowspan: number) {
        this.irowspan = irowspan;
    }
    getiColSpan(): number {
        return this.icolspan;
    }
    setiColSpan(icolspan: number) {
        this.icolspan = icolspan;
    }
    setCellMove(td: HTMLElement, cell: Cell) {
        if (!this.mousedn) {
            return;
        }
        // console.log("colspan = " + cell.colspan + ", rowspan = " + cell.rowspan);
        if (cell.colspan > 1 || cell.rowspan > 1) {
            return;
        }
        let cellComp: CellComponent = this.currTableComp.getCellComp(cell);
        for (var i = 0; i < this.cellRange.length; i++) {
            this.cellRange[i].unhighlight();
        }
        this.cellRange = this.currTableComp.getRange(this.currCellComp.cell, cellComp.cell);
        // console.log("cellRange.length = " + this.cellRange.length);
        for (var i = 0; i < this.cellRange.length; i++) {
            this.cellRange[i].highlight();
        }
    }

    setiCellMove(td: HTMLElement, cell: Cell) {
        if (!this.mousedn) {
            return;
        }
        // console.log("colspan = " + cell.colspan + ", rowspan = " + cell.rowspan);
        if (cell.colspan > 1 || cell.rowspan > 1) {
            return;
        }
        let icellComp: CellComponent = this.currITableComp.getCellComp(cell);
        for (var i = 0; i < this.cellRange.length; i++) {
            this.icellRange[i].unhighlight();
        }
        this.icellRange = this.currITableComp.getRange(this.curriCellComp.cell, icellComp.cell);
        // console.log("icellRange.length = " + this.icellRange.length);
        for (var i = 0; i < this.icellRange.length; i++) {
            this.icellRange[i].highlight();
        }
    }

    setiMerge(imerge: boolean) {
        this.imerge = imerge;
        // console.log("imerge = " + this.imerge);
    }

    getiMerge(): boolean {
        return this.imerge;
    }

    getCellRange(): CellComponent[] {
        return this.cellRange;
    }

    getiCellRange(): CellComponent[] {
        return this.icellRange;
    }

    getDeviceType(): string {
        return this.deviceType;
    }

    setDeviceType(type: string) {
        this.deviceType = type;
    }

    setMobileCxFormComp(mobileFormComp: MobilecxformComponent) {
        this.mobileFormComp = mobileFormComp;
    }

    getMobileCxFormComp() {
        return this.mobileFormComp;
    }

}