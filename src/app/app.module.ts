import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainwinComponent } from './mainwin/mainwin.component';
import { BotbarComponent } from './botbar/botbar.component';
import { CxformComponent } from './cxforms/cxform/cxform.component';
import { FormsModule } from '@angular/forms';

import { ToolbarService } from './toolbar/toolbar.service';

import { DoundoService } from './command/doundo.service';

import { PageComponent } from './cxforms/cxform/page/page.component';
import { TableComponent } from './cxforms/cxform/table/table.component';
import { ScService } from './sc.service';
import { SidebarlistComponent } from './sidebar/sidebarlist/sidebarlist.component';
import { RowComponent } from './cxforms/cxform/table/row/row.component';
import { CellComponent } from './cxforms/cxform/table/cell/cell.component';
import { SettingsComponent } from './toolbar/settings/settings.component';
import { Routes, RouterModule } from '@angular/router';
import { Page } from './cxforms/shared/page.model';
import { ContextmenuComponent } from './cxforms/cxform/contextmenu/contextmenu.component';
import { TxtelComponent } from './cxforms/cxform/table/cellel/txtel/txtel.component';
import { CelldirectiveDirective } from './cxforms/cxform/table/cell/celldirective.directive';
import { CellElDirective } from './cxforms/cxform/table/cellel/celleldirective.directive';
import { ImgelComponent } from './cxforms/cxform/table/cellel/imgel/imgel.component';
import { InpelComponent } from './cxforms/cxform/table/cellel/inpel/inpel.component';
import { TaelComponent } from './cxforms/cxform/table/cellel/tael/tael.component';
import { OperationsComponent } from './operations/operations.component';
import { AssetlistComponent } from './operations/assetlist/assetlist.component';
import { AssettreeComponent } from './assettree/assettree.component';
import { AssetlistitemComponent } from './operations/assetlist/assetlistitem/assetlistitem.component';
import { ElistitemformComponent } from './operations/assetlist/assetlistitem/elistitemform/elistitemform.component';
import { TreenodeComponent } from './assettree/treenode/treenode.component';
import { FormnodeComponent } from './assettree/treenode/formnode/formnode.component';
import { AccountadminComponent } from './toolbar/accountadmin/accountadmin.component';
import { AssetreflistComponent } from './operations/assetreflist/assetreflist.component';
import { AssetreflistitemComponent } from './operations/assetreflist/assetreflistitem/assetreflistitem.component';
import { EreflistitemformComponent } from './operations/assetreflist/assetreflistitem/ereflistitemform/ereflistitemform.component';
import { CxformrefComponent } from './cxforms/cxformref/cxformref.component';
import { ItelComponent } from './cxforms/cxform/table/cellel/itel/itel.component';
import { HrelComponent } from './cxforms/cxform/table/cellel/hrel/hrel.component';
import { MobilemainComponent } from './mobile/mobilemain.component';
import { MobilecxformComponent } from './mobile/mobilecxform/mobilecxform.component';

const routes: Routes = [
  {path: 'mainwin/:id1/:id2', component: MainwinComponent},
  {path: 'mainwin/:id', component: MainwinComponent},
  {path: 'mainwin', component: MainwinComponent},
  {path: 'mobilemain', component: MobilemainComponent},
  {path: '', component: MainwinComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    SidebarComponent,
    MainwinComponent,
    BotbarComponent,
    CxformComponent,
    PageComponent,
    TableComponent,
    SidebarlistComponent,
    RowComponent,
    CellComponent,
    SettingsComponent,
    ContextmenuComponent,
    TxtelComponent,
    CelldirectiveDirective,
    CellElDirective,
    ImgelComponent,
    InpelComponent,
    TaelComponent,
    OperationsComponent,
    AssetlistComponent,
    AssettreeComponent,
    AssetlistitemComponent,
    ElistitemformComponent,
    TreenodeComponent,
    FormnodeComponent,
    AccountadminComponent,
    AssetreflistComponent,
    AssetreflistitemComponent,
    EreflistitemformComponent,
    CxformrefComponent,
    ItelComponent,
    HrelComponent,
    MobilemainComponent,
    MobilecxformComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireDatabaseModule,
    FormsModule,
    RouterModule.forRoot(
      routes, { enableTracing: false }
    )
  ],
  providers: [
    ToolbarService,
    ScService,
    DoundoService,
    Page
  ],
  entryComponents: [
    CxformComponent,
    PageComponent,
    TableComponent,
    RowComponent,
    SettingsComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}