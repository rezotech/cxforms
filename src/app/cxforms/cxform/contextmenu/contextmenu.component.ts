import { Component, OnInit, Input } from '@angular/core';
import { ToolbarService } from '../../../toolbar/toolbar.service';
import { ScService } from '../../../sc.service';
import { Cellel } from '../../shared/cellel.model';

@Component({
  selector: 'app-contextmenu',
  templateUrl: './contextmenu.component.html',
  styleUrls: ['./contextmenu.component.css']
})
export class ContextmenuComponent implements OnInit {
  
  constructor(
    private toolbarService: ToolbarService,
    private scService: ScService
  ) { }

  @Input() x=0;
  @Input() y=0;
  @Input() inner: boolean;

  ngOnInit() {
  }

  merge() {
    if(!this.inner) {
      this.toolbarService.doMerge();
    } else {
      this.toolbarService.doiMerge();
    }
  }
  unmerge() {
    if(!this.inner) {
      this.toolbarService.doUnMerge();
    } else {
      this.toolbarService.doiUnMerge();
    }
  }
  copypage() {
    let page = JSON.parse(JSON.stringify(this.scService.getCurrPageComp().page));
    this.scService.setClipPageEl(page);
    // window.alert("copypage()!");

  }
  pastepage() {
    if(!this.scService.getCxForm() || !this.scService.getClipPageEl()) {
      return;
    }
    const pageNo = this.scService.getCxForm().no_pages + 1;
    this.scService.getCxForm().no_pages = pageNo;
    this.scService.getClipPageEl().pageno = pageNo;

    this.toolbarService.insertPage(this.scService.getClipPageEl());
    this.toolbarService.updateCxform(this.scService.getCxForm());

  }
  copytable() {
    let table = JSON.parse(JSON.stringify(this.scService.getCurrTableComp().table));
    this.scService.setClipTableEl(table);
    // window.alert("copypage()!");

  }
  pastetable() {
    // window.alert("pastetable()!");
    if(!this.scService.getCurrTableComp() || !this.scService.getClipTableEl()) {
      return;
    }
    const indexNo = this.scService.getCurrPageComp().page.tables.indexOf(this.scService.getCurrTableComp().table);
    this.scService.getCurrPageComp().page.tables.splice(indexNo, 0, this.scService.getClipTableEl());

    this.toolbarService.updatePage(this.scService.getCurrPageComp().page); 
    
  }
  copycell() {
    let cellels = this.scService.getCurrCellComp().cell.cellels;
    let clipCellEls: Cellel[] = [];
    for(let i=0; i<cellels.length; i++) {
      let newCellEl = JSON.parse(JSON.stringify(cellels[i]));
      clipCellEls.push(newCellEl);
    }
    this.scService.setClipCellEls(clipCellEls);
    // window.alert("copycell()! length = " + this.scService.getClipCellEls().length);

  }
  pastecell() {
    // window.alert("pastecell()! length = " + this.scService.getClipCellEls().length);
    if(!this.scService.getCurrCellComp() || !this.scService.getClipCellEls()) {
      return;
    }
    let newCellEls = this.scService.getCurrCellComp().cell.cellels;
    newCellEls.length = 0;
    for(let i=0; i<this.scService.getClipCellEls().length; i++) {
      newCellEls.push(this.scService.getClipCellEls()[i]);
    }
    this.toolbarService.updatePage(this.scService.getCurrPageComp().page);
   
  }
  
}
