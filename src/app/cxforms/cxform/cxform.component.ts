import {
  Component,
  OnInit,
  Inject,
  ChangeDetectorRef,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Cxform } from '../shared/cxform.model';
import { Page } from '../shared/page.model';
import { ScService } from '../../sc.service';
import { PageComponent } from '../cxform/page/page.component';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ContextmenuComponent } from '../cxform/contextmenu/contextmenu.component';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { DocumentSnapshot } from '@firebase/firestore-types';

import 'rxjs/add/operator/take';

@Component({
  selector: 'app-cxform',
  templateUrl: './cxform.component.html',
  styleUrls: ['./cxform.component.css']
})

export class CxformComponent {
  private pagesCollection: AngularFirestoreCollection<Page>;
  _pages: Observable<Page[]>;
  pages: Page[] = [];
  pagenos: Number[] = [];
  cxformDoc: AngularFirestoreDocument<Cxform>;
  cxform: Observable<Cxform>;
  form: Cxform;
  name: String;
  page: Page;

  constructor(
    private scService: ScService,
    private readonly afs: AngularFirestore,
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef
  ) {
    this.name = "Form";
    // this.route.params.subscribe(params => {
    //   this.key = params['id'];
    //   console.log('form key = ' + this.key);
    // });
    // console.log("key = " + this.key);

    this.scService.cxForm_messageSource.subscribe((cxForm => {
      if (cxForm != null) {
        this.pages.length = 0;
        this.pagenos.length = 0;
        let key = cxForm.$key;
        console.log('form key = ' + key);
        if (key) {
          this.pagesCollection = afs.collection<Page>('pages', ref => ref.where('formKey', '==', key).orderBy('pageno'));
          this._pages = this.pagesCollection.snapshotChanges().map(action => {
            return action.map(a => {
              const page = a.payload.doc.data() as Page;
              page.$key = a.payload.doc.id;
              return { ...page };
            })
          });
          this._pages.subscribe(ps => {
            let i = 0;
            this.pages.length = 0;
            ps.forEach(p => {
              console.log("page no = " + p.pageno);
              if (i < 2 && p.pageno>2) {
                console.log("i = " + i);
                this.pages.push(p);
                i++;
              }
            });
          });
        }
      }
    }));

    // this.route.params.subscribe(params => {
    //   this.pages.length = 0;
    //   this.pagenos.length = 0;
    //   let key = params['id'];
    //   console.log('form key = ' + key);
    //   if (key) {
    //     this.cxformDoc = this.afs.doc<Cxform>('forms/' + key);
    //     this.cxformDoc.ref.get().then(ss => {
    //       this.form = ss.data() as Cxform;
    //       console.log("id = " + this.form.$key);
    //       console.log("page_nos = " + this.form.no_pages);
    //       for (var i = 0; i < this.form.no_pages; i++) {
    //         this.pagenos.push(i+1);
    //       }
    //     });
    //   }
    // });

    // this.cxformDoc.snapshotChanges().subscribe(
    //   (res) => {
    //     console.log(res.payload)
    //     // this.form = res.payload.data() as Cxform;
    //     // console.log("nopages = " + this.form.no_pages);
    //     if(res!==null) {
    //       this.form.$key = key;
    //     }
    //     this.scService.setCxform(this.form);
    //   },
    //   (err) => console.log(err),
    //   // () => console.log('done!')
    // )

    // var pagesReference = afs.collection<Page>('pages');
    // var query = pagesReference.ref.where('formKey', '==', key).orderBy('pageno');
    // query.get().then(querySnapshot => {
    //   querySnapshot.forEach(documentSnapshot => {
    //     const page = documentSnapshot.data() as Page;
    //     page.$key = documentSnapshot.id;
    //     console.log("formKey = " + page.formKey);
    //     console.log("key = " + page.$key);
    //     console.log("table.length = " + page.tables.length);
    //     this.pages.push(page);
    //   });
    // });

  }

  detachChanges() {
    this.ref.detach();
  }

  attachChanges() {
    this.ref.reattach();
  }

  mouseover() {
    this.scService.setCurrCxformComp(this);
  }

  mousedown() {
    // console.log("mousedown()");
    this.scService.mouseDown(true);
  }

  mouseup() {
    // console.log("mouseup()");
    this.scService.mouseDown(false);
  }

  mouseleave() {
    // console.log("mouseleave()");
    this.scService.mouseDown(false);
  }

}