import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Input,
  Inject,
  ViewChild,
  ViewChildren,
  ElementRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  QueryList,
  AfterViewInit,
  HostListener,
  Renderer2
} from '@angular/core';
import { Table } from '../../shared/table.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { ScService } from '../../../sc.service';
import { Observable } from 'rxjs';
import { Row } from '../../shared/row.model';
import { RowComponent } from './row/row.component';
import { CellComponent } from './cell/cell.component';
import { Cell } from '../../shared/cell.model';

@Component({
  selector: '[app-table]',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, AfterViewInit {
  @Input('table') table: Table;
  @Input('master') masterName: string;
  factoryResolver: ComponentFactoryResolver;
  viewContainerRef: ViewContainerRef;
  @ViewChild("tcomp", { read: ElementRef }) compEl: ElementRef;
  @ViewChild("vc", { read: ViewContainerRef }) vc: ViewContainerRef;
  @ViewChildren(RowComponent) rows_: QueryList<RowComponent>;
  private rows: RowComponent[];

  constructor(
    @Inject(ComponentFactoryResolver) factoryResolver,
    private scService: ScService,
    private readonly afs: AngularFirestore,
    viewContainerRef: ViewContainerRef,
    public renderer: Renderer2) 
  {
    this.factoryResolver = factoryResolver;
    this.viewContainerRef = viewContainerRef;
  }

  click(e : Event) {
    if (this.scService.getCurrTableComp()) {
      this.scService.getCurrTableComp().compEl.nativeElement.classList.remove("active");
    }
    this.scService.setCurrTableComp(this);
    this.scService.getCurrTableComp().compEl.nativeElement.classList.add("active");
    // console.log("table click rows count = " + this.rows.length);

  }

  mouseover(e : Event) {
    console.log("mouseover");
//    console.log("table mouseover rows count = " + this.rows.length);
  }

  @HostListener('mousedown') onHover() {
    // console.log("mousedown");
    this.scService.setCurrTableComp(this);
  }

  mousedown(e : Event) {
//     console.log("mousedown");
//     if (this.scService.getCurrTableComp()) {
//       this.scService.getCurrTableComp().compEl.nativeElement.classList.remove("active");
//     }
//     this.scService.setCurrTableComp(this);
//     this.scService.getCurrTableComp().compEl.nativeElement.classList.add("active");
// //    console.log("table mousedown rows count = " + this.rows.length);
  }

  rowClick(e : Event, row: Row) {
    // console.log("rowClick()");
    // this.scService.setCurrRow(row);
    // e.srcElement.classList.add("rowactive");
  }

  getCellComp(cell: Cell): CellComponent {
    console.log("row="+cell.row +",col="+cell.col);
    return this.rows[cell.row].cells[cell.col];
  }

  getRange(start : Cell, end : Cell): CellComponent[] {
    console.log("getRange()! start = " + start.row + ", " + start.col);
    console.log("getRange()! end = " + end.row + ", " + end.col);
    let range : CellComponent[] = [];
    var up : boolean = false;
    var dn : boolean = false;
    var left : boolean = false;
    var right : boolean = false;
   
    if(start.row>end.row) {
//      console.log("Up");
      this.scService.setRowSpan(start.row-end.row+1);
      up = true;
    } else {
//      console.log("Down");
      this.scService.setRowSpan(end.row-start.row+1);
      dn = true;
    }
    if(start.col>end.col) {
//      console.log("Left");
      this.scService.setColSpan(start.col-end.col+1);
      left = true;
    } else {
//      console.log("Right");
      this.scService.setColSpan(end.col-start.col+1);
      right = true;
    }

    if(up&&right) {
      for(var i=end.row; i<=start.row; i++) {
//        console.log("i=" + i);
//        console.log("end.col=" + end.col + ",start.col=" + start.col);
        var row : RowComponent = this.rows[i];
        for(var j=start.col; j<=end.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          range.push(cell);
        }
      }
    }
    if(up&&left) {
      for(var i=end.row; i<=start.row; i++) {
//        console.log("i=" + i);
//        console.log("end[1]=" + end.col + ",start[1]=" + start.col);
        var row : RowComponent = this.rows[i];
        for(var j=end.col; j<=start.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          range.push(cell);
        }
      }
    }
    if(dn&&right) {
      for(var i=start.row; i<=end.row; i++) {
//        console.log("i=" + i);
//        console.log("end.col=" + end.col + ",start.col=" + start);
        var row : RowComponent = this.rows[i];
        for(var j=start.col; j<=end.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          range.push(cell);
        }
      }
    }
    if(dn&&left) {
      for(var i=start.row; i<=end.row; i++) {
//        console.log("i=" + i);
//        console.log("end.col=" + end.col + ",start.col=" + start.col);
        var row : RowComponent = this.rows[i];
        for(var j=end.col; j<=start.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          range.push(cell);
        }
      }
    }

    return range;
  }

  unMerge(row:number,col:number,rowspan:number,colspan:number) {
//    console.log(row,col,colspan,rowspan);
    for(let i=row; i<(row+rowspan); i++) {
      var row_ : RowComponent = this.rows[i];
      for(let j=col; j<(col+colspan); j++) {
        var cell_ : CellComponent = row_.cells[j];
        cell_.cell.width = this.table.cols[col];
        cell_.cell.height = row_.row.height;
        cell_.cell.rowspan=1;
        cell_.cell.colspan=1;
        cell_.cell.display="";
      }
    }
  }

  ngOnInit() {
    // console.log("table component ngOnInit rows = " + this.table.rows.length);
  }

  ngAfterViewInit(): void {
    this.rows = this.rows_.toArray();
    var width = 0;
    for(var i=0; i<this.table.cols.length; i++) {
      width += this.table.cols[i];
    }
    this.renderer.setStyle(this.compEl.nativeElement, "width", width + "px");
  }
}
