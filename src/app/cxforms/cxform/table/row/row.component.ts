import { Component, OnInit, Input, ViewChild, ViewChildren, QueryList, ElementRef, AfterViewInit, Host, Self, HostListener } from '@angular/core';
import { Row } from '../../../shared/row.model';
import { Cell } from '../../../shared/cell.model';
import { ScService } from '../../../../sc.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { CellComponent } from '../cell/cell.component';
import { CelldirectiveDirective } from '../cell/celldirective.directive';
import { Table } from '../../../shared/table.model';

@Component({
  selector: '[app-row]',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.css']
})
export class RowComponent implements OnInit, AfterViewInit {
  @Input() table: Table;
  @Input() row: Row;
  @Input() inner: boolean;
  @Input('master') masterName: string;
  @ViewChild("comp", { read: ElementRef }) compEl: ElementRef;
  @ViewChildren(CellComponent) cellComps: QueryList<CellComponent>
  cells: CellComponent[];
  private cellsCollection: AngularFirestoreCollection<Cell>;
  name: String;

  constructor(
    private scService: ScService,
    private readonly afs: AngularFirestore
  ) {
    this.name = 'Row';
  }

  click(e: Event, cell) {
    // if(this.scService.getCurrRowComp()) {
    //   this.scService.getCurrRowComp().compEl.nativeElement.style.backgroundColor="transparent";
    // }
    // this.compEl.nativeElement.style.backgroundColor = "pink";
    // e.srcElement.classList.add("highlight");

    // console.log("row click cell = " + cell.col + ", " + cell.row);
    if (!this.inner) {
      // console.log("Setting curr outercell");
      this.scService.setCurrCell(<HTMLElement>e.currentTarget, cell);
    } else {
      // console.log("Setting curr innercell");
      this.scService.setCurrICell(<HTMLElement>e.currentTarget, cell);
    }
  }

  mouseover(e: Event, cell) {
    // console.log("mouseover");
    if (!this.inner) {
      this.scService.setCellMove(<HTMLElement>e.currentTarget, cell);
    } else {
      this.scService.setiCellMove(<HTMLElement>e.currentTarget, cell);
    }
  }

  @HostListener('mousedown') onHover() {
    this.scService.setCurrRow(this.row);
  }

  ngOnInit() {
    // this._tdContentQuery.addConnect();
  }

  ngAfterViewInit(): void {
    this.cells = this.cellComps.toArray();
  }

}
