import { Component, OnInit, Input, ViewChild, ElementRef, Host, Renderer, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { NgSwitch } from '@angular/common';
import { Cell } from '../../../shared/cell.model';
import { ScService } from '../../../../sc.service';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { CelldirectiveDirective } from './celldirective.directive';
import { Row } from '../../../shared/row.model';
import { Table } from '../../../shared/table.model';

@Component({
  selector: '[app-cell]',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css'],
})
export class CellComponent implements OnInit, AfterViewInit {
  @Input() table: Table;
  @Input() row: Row;
  @Input() cell: Cell;
  @ViewChild("contain", { read: ElementRef }) contain: ElementRef;
  private tablesCollection: AngularFirestoreCollection<Cell>;
  name: String;
 
  constructor(
    @Host() public cellDirective: CelldirectiveDirective,
    private scService: ScService, 
    private readonly afs: AngularFirestore,
    private renderer: Renderer
  ) {
    this.name = 'Cell';
  }

  ngOnInit() {
    // this.renderer.setElementStyle(this.tdEl, 'overflow', 'hidden');
  }

  ngAfterViewInit() {
 //   this.renderer.setElementStyle(this.tdEl, 'width', this.table.cols[this.cell.col] + "px");
 //   this.renderer.setElementStyle(this.tdEl, 'height', this.row.height + "px");  
  }

  click(e: Event, cell: Cell) {
    // console.log("type = " + e);
    this.scService.setCurrCellComp(this);
     console.log("cell click!");
  }

  mousedown(e) {
    // console.log("mousedown!");
    // this.scService.setCellDn(this);
  }

  mouseover(e) {
    // console.log("mouseover!");
    // this.scService.setCellMove(this);
  }

  mouseup(e) {
    // console.log("mouseup!");
    // this.scService.setCellUp(this);
  }

  context(e) {
    console.log("context!");
  }

  highlight() {
    this.cellDirective.highlight();
  }

  unhighlight() {
    this.cellDirective.unhighlight();
  }

}