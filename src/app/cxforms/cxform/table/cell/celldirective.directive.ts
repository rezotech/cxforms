import { Directive, HostBinding, ElementRef, Renderer, HostListener, Self, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[cxTd]'
})

export class CelldirectiveDirective {
  @HostBinding('style.background-color') private backgroundcolor: string;
  @HostBinding('class.highlight') selected: boolean = false;
  
  constructor() {
  }
  
  highlight() {
    // this.backgroundcolor = "#ff99cc";
    this.selected = true;
  }

  unhighlight() {
    // this.backgroundcolor = "transparent";
    this.selected = false;
  }

  // @HostListener('mouseover') onmouseover() {
  //   this.backgroundcolor = "yellow";
  //   console.log("onmouseover");
  // }

  // @HostListener('mouseleave') onmouseleave() {
  //   this.backgroundcolor = "transparent";
  // }

}
