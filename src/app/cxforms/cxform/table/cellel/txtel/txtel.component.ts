import { Component, OnInit, Input, Host, ChangeDetectionStrategy } from '@angular/core';
import { Cellel } from '../../../../shared/cellel.model';
import { CellElDirective } from '.././celleldirective.directive';
import { ScService } from '../../../../../sc.service';

@Component({
  selector: 'app-txtel',
  templateUrl: './txtel.component.html',
  styleUrls: ['./txtel.component.css'],
})
export class TxtelComponent implements OnInit {
  @Input() cellel: Cellel;

  constructor(
    @Host() public cellDirective: CellElDirective,
    private scService: ScService, 
  ) { }

  ngOnInit() {
  }

  click(e: Event) {
    // window.alert("text click!");
    this.scService.setCurrCellEl(this.cellel);
  }

}
