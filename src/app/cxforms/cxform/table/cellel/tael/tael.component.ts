import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Cellel } from '../../../../shared/cellel.model';
import { ScService } from '../../../../../sc.service';

@Component({
  selector: 'app-tael',
  templateUrl: './tael.component.html',
  styleUrls: ['./tael.component.css'],
})
export class TaelComponent implements OnInit {
  @Input() cellel: Cellel;

  constructor(private scService: ScService) { }

  ngOnInit() {
    if(this.scService.getCurrMode()=="form") {
      console.log("form mode");
    } else if(this.scService.getCurrMode()=="formref") {
      console.log("formref mode");
    }
  }

}
