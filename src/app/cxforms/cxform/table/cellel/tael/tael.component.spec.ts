import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaelComponent } from './tael.component';

describe('TaelComponent', () => {
  let component: TaelComponent;
  let fixture: ComponentFixture<TaelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
