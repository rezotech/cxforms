import { Component, OnInit, Input, Renderer2, ChangeDetectionStrategy } from '@angular/core';
import { Cellel } from '../../../../shared/cellel.model';
import { ScService } from '../../../../../sc.service';

@Component({
  selector: 'app-hrel',
  templateUrl: './hrel.component.html',
  styleUrls: ['./hrel.component.css'],
})
export class HrelComponent implements OnInit {
  @Input() cellel: Cellel;

  constructor(
    private scService: ScService,
    public renderer: Renderer2
  ) { }

  click(e: Event) {
    // alert("click! x " + this.cellel.text);
    this.scService.setCurrCellEl(this.cellel);
  }

  ngOnInit() {
  }

}
