import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrelComponent } from './hrel.component';

describe('HrelComponent', () => {
  let component: HrelComponent;
  let fixture: ComponentFixture<HrelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
