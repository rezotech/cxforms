import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Cellel } from '../../../../shared/cellel.model';

@Component({
  selector: 'app-imgel',
  templateUrl: './imgel.component.html',
  styleUrls: ['./imgel.component.css'],
})
export class ImgelComponent implements OnInit {
  @Input() cellel: Cellel;

  constructor() { }

  ngOnInit() {
  }

}
