import { ChangeDetectionStrategy, Component, OnInit, Input, ViewChild, ElementRef, Renderer, ChangeDetectorRef } from '@angular/core';
import { Cellel } from '../../../../shared/cellel.model';
import { ScService } from '../../../../../sc.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { Inputel } from '../../../../shared/formref.model';
import { ToolbarService } from '../../../../../toolbar/toolbar.service';

@Component({
  selector: 'app-inpel',
  templateUrl: './inpel.component.html',
  styleUrls: ['./inpel.component.css']
})
export class InpelComponent implements OnInit {
  @Input() cellel: Cellel;
  @ViewChild("inel", { read: ElementRef }) inputEl: ElementRef;

  constructor(private scService: ScService,
    private afs: AngularFirestore,
    private renderer: Renderer,
    private toolbarservice: ToolbarService,
  ) { }

  inputClick(e: Event) {
    //this.scService.setCurrCellEl(this.cellel);
  }

  inputBlur(e: Event) {
    console.log("inputBlur()");
    this.scService.getCurrCxformComp().detachChanges();
    if (this.scService.getCurrMode() == "form") {
      console.log("form! " + this.inputEl.nativeElement.value.trim());
      this.cellel.value = this.inputEl.nativeElement.value.trim();
      console.log("test 1")
      var page = this.scService.getCurrPage();
      console.log("page 2 key = " + page.$key);
      this.afs.collection('pages').doc(page.$key).set(JSON.parse(JSON.stringify(page)));
    } else if (this.scService.getCurrMode() == "formref") {
      console.log("formref!");
      var formref = this.scService.getCurrFormref();
      if (!formref.inputs) {
        formref.inputs = [];
      }
      var iEl = null;
      for (var i = 0; i < formref.inputs.length; i++) {
        var el = formref.inputs[i];
        if (el.id == this.cellel.input_no) {
          iEl = el;
          break;
        }
      }
      if (iEl == null) {
        iEl = new Inputel();
        iEl.id = this.cellel.input_no;
        iEl.value = this.inputEl.nativeElement.value.trim();
        formref.inputs.push(iEl);
        this.afs.collection('formrefs').doc(formref.$key).set(JSON.parse(JSON.stringify(formref)));
      } else {
        iEl.value = this.inputEl.nativeElement.value.trim();
        this.afs.collection('formrefs').doc(formref.$key).set(JSON.parse(JSON.stringify(formref)));
      }
    }
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    // this.renderer.setElementStyle(this.inputEl, "width", this.cellel.width + "");
    // this.renderer.setElementStyle(this.inputEl, "height", this.cellel.height + "");
    if (this.scService.getCurrMode() == "form") {
      //console.log("form mode");
      this.inputEl.nativeElement.value = this.cellel.value;
    } else if (this.scService.getCurrMode() == "formref") {
      // console.log("formref mode");
      var ival = "ival";
      var formref = this.scService.getCurrFormref();
      // console.log("inputs length = " + formref.inputs.length);
      for (var i = 0; i < formref.inputs.length; i++) {
        var iput = formref.inputs[i];
        // console.log(i + ", " + formref.inputs[i].id);
        // console.log("cell input_no = " + this.cellel.input_no);
        if (iput.id == this.cellel.input_no) {
          ival = iput.value;
          break;
        }
      }
      this.inputEl.nativeElement.value = ival;
    }
  }

}
