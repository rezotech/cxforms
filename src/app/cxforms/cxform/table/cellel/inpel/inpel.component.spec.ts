import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InpelComponent } from './inpel.component';

describe('InpelComponent', () => {
  let component: InpelComponent;
  let fixture: ComponentFixture<InpelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InpelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InpelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
