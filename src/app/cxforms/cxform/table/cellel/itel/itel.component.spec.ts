import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItelComponent } from './itel.component';

describe('ItelComponent', () => {
  let component: ItelComponent;
  let fixture: ComponentFixture<ItelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
