import { Component, OnInit, Input, ViewChildren, QueryList, ViewChild, ElementRef, Renderer2, ChangeDetectionStrategy, } from '@angular/core';
import { Cellel } from '../../../../shared/cellel.model';
import { ScService } from '../../../../../sc.service';
import { Cell } from '../../../../shared/cell.model';
import { CellComponent } from '../../cell/cell.component';
import { RowComponent } from '../../row/row.component';

@Component({
  selector: 'app-itel',
  templateUrl: './itel.component.html',
  styleUrls: ['./itel.component.css'],
})
export class ItelComponent implements OnInit {
  @Input() cellel: Cellel;
  @ViewChild("tcomp", { read: ElementRef }) compEl: ElementRef;
  @ViewChildren(RowComponent) rows_: QueryList<RowComponent>;
  private rows: RowComponent[];

  constructor(
    private scService: ScService,
    public renderer: Renderer2
  ) { }

  getCellComp(cell: Cell): CellComponent {
    if(!this.rows[cell.row]) {
      return;
    }
    return this.rows[cell.row].cells[cell.col];
  }

  getRange(start : Cell, end : Cell): CellComponent[] {
    console.log("getRange()! start = " + start.row + ", " + start.col);
    console.log("getRange()! end = " + end.row + ", " + end.col);
    let range : CellComponent[] = [];
    var up : boolean = false;
    var dn : boolean = false;
    var left : boolean = false;
    var right : boolean = false;
    var height : number = 0;
    var width : number = 0;

    if(start.row>end.row) {
//      console.log("Up");
      this.scService.setiRowSpan(start.row-end.row+1);
      up = true;
    } else {
//      console.log("Down");
      this.scService.setiRowSpan(end.row-start.row+1);
      dn = true;
    }
    if(start.col>end.col) {
//      console.log("Left");
      this.scService.setiColSpan(start.col-end.col+1);
      left = true;
    } else {
//      console.log("Right");
      this.scService.setiColSpan(end.col-start.col+1);
      right = true;
    }

    if(up&&right) {
      for(var i=end.row; i<=start.row; i++) {
//        console.log("i=" + i);
//        console.log("end.col=" + end.col + ",start.col=" + start.col);
        var row : RowComponent = this.rows[i];
        height = height + row.row.height;
        for(var j=start.col; j<=end.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          width = width + cell.cell.width;
          range.push(cell);
        }
      }
    }
    if(up&&left) {
      for(var i=end.row; i<=start.row; i++) {
//        console.log("i=" + i);
//        console.log("end[1]=" + end.col + ",start[1]=" + start.col);
        var row : RowComponent = this.rows[i];
        height = height + row.row.height;
        for(var j=end.col; j<=start.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          width = width + cell.cell.width;
          range.push(cell);
        }
      }
    }
    if(dn&&right) {
      for(var i=start.row; i<=end.row; i++) {
//        console.log("i=" + i);
//        console.log("end.col=" + end.col + ",start.col=" + start);
        var row : RowComponent = this.rows[i];
        height = height + row.row.height;
        for(var j=start.col; j<=end.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          width = width + cell.cell.width;
          range.push(cell);
        }
      }
    }
    if(dn&&left) {
      for(var i=start.row; i<=end.row; i++) {
//        console.log("i=" + i);
//        console.log("end.col=" + end.col + ",start.col=" + start.col);
        var row : RowComponent = this.rows[i];
        height = height + row.row.height;
        for(var j=end.col; j<=start.col; j++) {
//          console.log("j=" + j);
          var cell : CellComponent = row.cells[j];
          width = width + cell.cell.width;
          range.push(cell);
        }
      }
    }

    return range;
  }

  unMerge(row:number,col:number,rowspan:number,colspan:number) {
//    console.log(row,col,colspan,rowspan);
    for(let i=row; i<(row+rowspan); i++) {
      var row_ : RowComponent = this.rows[i];
      for(let j=col; j<(col+colspan); j++) {
        var cell_ : CellComponent = row_.cells[j];
        cell_.cell.rowspan=1;
        cell_.cell.colspan=1;
        cell_.cell.display="";
      }
    }
  }

  leave(e: Event) {
    this.scService.setiMerge(false);
  }

  over(e: Event) {
    this.scService.setiMerge(true);
  }

  click(e: Event) {
    this.scService.setCurrITableComp(this);
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.rows = this.rows_.toArray();
    var width = 0;
    for(var i=0; i<this.cellel.table.cols.length; i++) {
      width += this.cellel.table.cols[i];
    }
    this.renderer.setStyle(this.compEl.nativeElement, "width", width + "px");
  }

}
