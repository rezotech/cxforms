import { Directive, HostBinding, ElementRef, Renderer, HostListener, Self, ViewContainerRef } from '@angular/core';
import { JsonpClientBackend } from '@angular/common/http';

@Directive({
  selector: '[cxEl]'
})

export class CellElDirective {
  @HostBinding('style.background-color') private backgroundcolor: string;
  
  constructor() {
  }

  highlight() {
    this.backgroundcolor = "#ff99cc";
  }

  unhighlight() {
    this.backgroundcolor = "transparent";
  }

  // @HostListener('mouseover') onmouseover() {
  //   this.backgroundcolor = "yellow";
  //   console.log("onmouseover");
  // }

  // @HostListener('mouseleave') onmouseleave() {
  //   this.backgroundcolor = "transparent";
  // }

  // @HostListener('click') onclick() {
    
  // }

}
