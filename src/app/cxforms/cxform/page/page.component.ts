import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Input,
  Inject,
  AfterViewInit,
  ChangeDetectorRef,
} from '@angular/core';
import { ScService } from '../../../sc.service';
import { Page } from '../../shared/page.model';
import { TableComponent } from '../table/table.component';
import { Table } from '../../shared/table.model';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import 'rxjs/add/operator/take';

@Component({
  selector: '[app-page]',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  @Input() page: Page = new Page();
  masterName: String;
  @ViewChild("comp", { read: ElementRef }) compEl: ElementRef;

  name: string;
  key: string;

  constructor(
    private scService: ScService,
    private readonly afs: AngularFirestore,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
  ) {
    this.name = "Page";

  }

  ngAfterViewInit() {
    // this.cdr.detach();
  }

  ngOnInit() { 
    // this.pageObs = this.afs.collection<Page>('pages', ref => ref.where('formKey', '==', this.key).where('pageno', '==', this.pageno).limit(1)).valueChanges();
    // this.pageObs.subscribe(pages => {
    //   console.log("Size = " + pages.length);
    //   if(pages.length==0) {
    //     return;
    //   }
    //   console.log("key = " + pages[0].$key);
    //   this.page = pages[0];
    // });
    // this.scService.setCurrPage(this.page);
    
    // console.log("page component ngOnInit key = " + this.page.$key);
    // console.log("page component tables = " + this.page.tables.length);
    // this.cdr.detach();

    // if (this.scService.getCurrPageComp()) {
    //   // console.log("getCurrPageComp() key = " + this.scService.getCurrPageComp().page.$key);
    //   // console.log("key = " + this.page.$key);
    //   if (this.scService.getCurrPageComp().page.$key == this.page.$key) {
    //     console.log("Here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    //   //  this.compEl.nativeElement.classList.add("active");
    //   }
    // } else {
    //   // console.log("getCurrPageComp() ========== null!!!!!!!!!!");
    // }

  }

  mouseover() {
    // console.log("onmouseover()");
  }

  click() {
    console.log("click page.$key = " + this.page.$key);
    this.scService.setCurrPageComp(this);
    // console.log("click()");
    // // if (this.scService.getCurrPageComp()) {
    // //   this.scService.getCurrPageComp().compEl.nativeElement.classList.remove("active");
    // // }
    // console.log("page.$key = " + this.page.$key);
    // this.scService.setCurrPage(this.page);
    // this.scService.getCurrPageComp().compEl.nativeElement.classList.add("active");
    // // console.log("page no = " + this.page.pageno);
    // // console.log("# tables = " + this.page.tables.length);
  }

}
