import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxformComponent } from './cxform.component';

describe('CxformComponent', () => {
  let component: CxformComponent;
  let fixture: ComponentFixture<CxformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
