import { Row } from './row.model';

export class Table {
    $key: string;
    no: number;
    colunits: 'pt';
    cols: number[] = [];
    rows: Row[] = [];
}
