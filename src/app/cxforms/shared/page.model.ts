import { Table } from './table.model';

export class Page {
    $key: string;
    formKey: string;
    pageno: number;
    pwidth: string = "750px";
    pheight: string = "950px";
    tables: Table[] = [];
    createTime: string;
    updateTime: string;
}
