import { Cell } from './cell.model';

export class Row {
    $key: string;
    row: number;
    height: number;
    cells: Cell[] = [];
}
