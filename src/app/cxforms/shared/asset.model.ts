import { Cxform } from "./cxform.model";

export class Asset {
    $key: string;
    project_key: string;
    name: string = "Asset";
    asset_id: string;
    type: string;
    trade: string;
    make: string;
    model: string;
    serial_no: string;
    forms: Cxform[];
    level1: string;
    level2: string;
    level3: string;
    level4: string;
    level5: string;
    
}
