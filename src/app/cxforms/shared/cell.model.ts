import { Cellel } from "./cellel.model";

export class Cell {
    $key: string;
    rowKey: string;
    row: number;
    col: number;
    colspan: number;
    rowspan: number;
    width: number;
    height: number;
    halign: string;
    valign: string;
    display: string;
    cellels: Cellel[] = [];
    
}
