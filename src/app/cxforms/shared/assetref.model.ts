import { Formref } from "./formref.model";

export class Assetref {
    $key: string;
    project_key: string;
    asset_key: string;
    name: string = "Asset";
    asset_id: string;
    type: string;
    trade: string;
    make: string;
    model: string;
    serial_no: string;
    formrefs: Formref[];
    level1: string;
    level2: string;
    level3: string;
    level4: string;
    level5: string;
    
}
