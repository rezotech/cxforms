import { Page } from './page.model';

export class Cxform {
    $key: string;
    asset_key: string;
    name: string;
    no_pages: number;
    no_inputs: number = 0;
    created_ts: number;

    constructor() {
        this.no_pages = 1;

    }

}
