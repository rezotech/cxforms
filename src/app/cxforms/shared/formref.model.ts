

export class Formref {
    $key: string;
    assetref_key: string;
    form_key: string;
    project_key: string;
    created_ts: number;
    inputs: Inputel[] = [];
    
    locale1: string;
    locale2: string;
    locale3: string;
    locale4: string;
    locale5: string;

    constructor() {
    }

}

export class Inputel {
    id: number;
    value: string;
}
