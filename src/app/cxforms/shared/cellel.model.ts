import { Table } from "./table.model";

export class Cellel {
    cellKey: string;
    elemNo: number;
    text: string;
    type: string;
    storageid: string;
    input_no: number;
    url: string;
    width: number;
    height: number;
    size: number;
    value: string;
    table: Table;
    ftsize: number;
    bold: boolean;
    italic: boolean;
    underline: boolean;

}