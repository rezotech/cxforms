import { Page } from './page.model';

export class Project {
    $key: string;
    account_key: string;
    name: string;
    no: string;
    subs: LevelItem[] = [];
    
    constructor() {
        
    }

}

export class LevelItem {
    $key: string;
    name: string = "LevelItem";
    project_key: string;
    subs: LevelItem[] = [];
}
