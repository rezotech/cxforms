import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxformrefComponent } from './cxformref.component';

describe('CxformrefComponent', () => {
  let component: CxformrefComponent;
  let fixture: ComponentFixture<CxformrefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxformrefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxformrefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
