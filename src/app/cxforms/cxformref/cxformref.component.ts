import {
  Component,
  OnInit,
  Inject,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Cxform } from '../shared/cxform.model';
import { Page } from '../shared/page.model';
import { ScService } from '../../sc.service';
import { PageComponent } from '../cxform/page/page.component';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ContextmenuComponent } from '../cxform/contextmenu/contextmenu.component';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Component({
  selector: 'app-cxformref',
  templateUrl: './cxformref.component.html',
  styleUrls: ['./cxformref.component.css']
})

export class CxformrefComponent {
  private pagesCollection: AngularFirestoreCollection<Page>;
  pages: Observable<Page[]>;
  cxformDoc: AngularFirestoreDocument<Cxform>;
  cxform: Observable<Cxform>;
  form: Cxform;
  name: String;
  page: Page;
  contextmenu = false;
  contextmenuX = 0;
  contextmenuY = 0;
 
  constructor(
    private scService: ScService,
    private readonly afs: AngularFirestore,
    private route: ActivatedRoute
  ) {
    this.name = "Form";

    this.route.params.subscribe(params => {
      let formrefkey = params['id1'];
      let formkey = params['id2'];
      // console.log('formkey = ' + formkey);
      if (formkey) {
        this.cxformDoc = this.afs.doc<Cxform>('cxforms/' + formkey);
        this.cxform = this.cxformDoc.valueChanges();
        this.pagesCollection = afs.collection<Page>('pages', ref => ref.where('formKey', '==', formkey).orderBy('pageno'));
        this.pages = this.pagesCollection.snapshotChanges().map(action => {
          return action.map(a => {
            const page = a.payload.doc.data() as Page;
            page.$key = a.payload.doc.id;
            // console.log("page no = " + page.pageno);
            // console.log("tables = " + page.tables.length);
            return { ...page };
          });
        });
        // this.cxform.subscribe(
        //   (res) => {
        //     // console.log(res)
        //     this.form = res;
        //     if(res!==null) {
        //       this.form.$key = formkey;
        //     }
        //     this.scService.setCxform(this.form);
        //   },
        //   (err) => console.log(err),
        //   // () => console.log('done!')
        // )
      }
    });
  }
  onrightClick(event) {
    // this.contextmenuX = (event.clientX - this.scService.getMainSplitEl().nativeElement.offsetLeft);
    // this.contextmenuY = (event.clientY - this.scService.getMainSplitEl().nativeElement.offsetTop);
    // console.log("screenX,Y = " + event.screenX + ", " + event.screenY);
    // console.log("clientX,Y = " + event.clientX + ", " + event.clientY);
    this.contextmenu = true;
  }
  disableContextMenu() {
    this.contextmenu = false;
  }

  mousedown() {
    this.scService.mouseDown(true);
  }

  mouseup() {
    this.scService.mouseDown(false);
  }

  mouseleave() {
    this.scService.mouseDown(false);
  }
 
}