import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobilecxformComponent } from './mobilecxform.component';

describe('MobilecxformComponent', () => {
  let component: MobilecxformComponent;
  let fixture: ComponentFixture<MobilecxformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobilecxformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobilecxformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
