import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestoreDocument, AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { Project } from '../cxforms/shared/project.model';
import { ScService } from '../sc.service';
import { ToolbarService } from '../toolbar/toolbar.service';
import { Asset } from '../cxforms/shared/asset.model';
import { TreeNode } from '../assettree/treenode/treenode.model';
import { Cxform } from '../cxforms/shared/cxform.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mobilemain',
  templateUrl: './mobilemain.component.html',
  styleUrls: ['./mobilemain.component.css']
})
export class MobilemainComponent implements OnInit {
  @ViewChild("shwmenudiv", { read: ElementRef }) shwmenudivEl: ElementRef;
  
  accounts: Observable<Asset[]>;

  projCollection: AngularFirestoreCollection<Project>;
  projects: Observable<Project[]>;
  assets: Observable<Asset[]>;

  project_name: string = "";
  project_key: string = "";
  user_name: string = "";

  enable_menu: boolean = false; 

  treeRoot: TreeNode = new TreeNode();

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private scService: ScService,
    private toolbarService: ToolbarService,
    private router: Router,
  ) {
    this.scService.project_messageSource.subscribe((proj => {
      // if(this.scService.getDeviceType()=="android") {
      //   this.router.navigate(['/mobilemainwin']);
      //   return;
      // }
      if (proj == null) {
        return;
      }
      console.log("proj Key = " + proj.$key);
      this.project_name = proj.name;
      this.project_key = proj.$key;
      this.assets = this.afs.collection<Asset>('assets', ref => ref.where('project_key', '==', proj.$key)).valueChanges();
      this.assets.subscribe(_assets => {
        this.upateAssets(_assets);
        // _assets.forEach(_asset => {
        //   console.log("asset.name = " + _asset.$key);
        // });
      });
    }));
    this.scService.user_messageSource.subscribe((user => {
      if (user != null) {
        this.user_name = user.name;
      }
    }));
    this.scService.account_messageSource.subscribe((account => {
      if (account != null) {
        console.log("account Key = " + account.$key);
        this.projects = this.afs.collection<Project>('projects', ref => ref.where('account_key', '==', account.$key)).valueChanges();
        this.projects.subscribe(projs => {
          projs.forEach(proj => {
            console.log("proj.name = " + proj.name);
          });
        });
      }
    }));
  }

  upateAssets(_assets: Asset[]) {
    console.log("updateAssets! size " + _assets.length);
    this.treeRoot = new TreeNode();
    _assets.forEach(_asset => {

      var forms = this.afs.collection<Cxform>('forms', ref => ref.where('asset_key', '==', _asset.$key)).valueChanges();
      forms.subscribe(_forms => {
        _asset.forms = _forms;
      });
      // Check Root
      if (_asset.level1 == null) {
        console.log("adding asset to root ");
        this.treeRoot.assets.push(_asset);
        return;
      }
      console.log("level1 = " + _asset.level1);
      var treeNode;
      var found: boolean = false;
      treeNode = this.treeRoot;
      treeNode.offset = 0;
      treeNode.level = 0;
      treeNode.children.forEach(_treeNode => {
        if (_treeNode.name == _asset.level1) {
          // console.log("Match!");
          treeNode = _treeNode;
          found = true;
        }
      });
      if (!found) {
        // console.log("Adding a new TreeNode()")
        var _treeNode = new TreeNode();
        _treeNode.offset = 0;
        _treeNode.level = 1;
        _treeNode.name = _asset.level1;
        treeNode.children.push(_treeNode);
        treeNode = _treeNode;
      }

      // level 2
      treeNode.offset = 0;
      if (_asset.level2 == null) {
        treeNode.assets.push(_asset);
        return;
      }
      console.log("level2 = " + _asset.level2);
      var found: boolean = false;
      treeNode.children.forEach(_treeNode => {
        if (_treeNode.name == _asset.level2) {
          console.log("Match!");
          treeNode = _treeNode;
          found = true;
        }
      });
      if (!found) {
        // console.log("Adding a new TreeNode()")
        _treeNode = new TreeNode();
        _treeNode.offset = 5;
        _treeNode.level = 2;
        _treeNode.name = _asset.level2;
        treeNode.children.push(_treeNode);
        treeNode = _treeNode;
      }

      // level 3
      treeNode.offset = 5;
      if (_asset.level3 == null) {
        treeNode.assets.push(_asset);
        return;
      }
      // console.log("level3 =" + _asset.level3);
      var found: boolean = false;
      treeNode.children.forEach(_treeNode => {
        if (_treeNode.name == _asset.level3) {
          console.log("Match!");
          treeNode = _treeNode;
          found = true;
        }
      });
      if (!found) {
        // console.log("Adding a new TreeNode()")
        _treeNode = new TreeNode();
        _treeNode.name = _asset.level3;
        _treeNode.level = 3;
        treeNode.children.push(_treeNode);
        treeNode = _treeNode;
      }

      // level 4
      treeNode.offset = 5;
      if (_asset.level4 == null) {
        treeNode.assets.push(_asset);
        return;
      }
      // console.log("level4 = " + _asset.level4);
      var found: boolean = false;
      treeNode.children.forEach(_treeNode => {
        if (_treeNode.name == _asset.level4) {
          // console.log("Match!");
          treeNode = _treeNode;
          found = true;
        }
      });
      if (!found) {
        // console.log("Adding a new TreeNode()")
        _treeNode = new TreeNode();
        _treeNode.name = _asset.level4;
        _treeNode.level = 4;
        treeNode.children.push(_treeNode);
        treeNode = _treeNode;
      }

      // level 5
      treeNode.offset = 5;
      if (_asset.level5 == null) {
        treeNode.assets.push(_asset);
        return;
      }
      // console.log("level5 = " + _asset.level5);
      var found: boolean = false;
      treeNode.children.forEach(_treeNode => {
        if (_treeNode.name == _asset.level5) {
          // console.log("Match!");
          treeNode = _treeNode;
          found = true;
        }
      });
      if (!found) {
        // console.log("Adding a new TreeNode()")
        _treeNode = new TreeNode();
        _treeNode.offset =5;
        _treeNode.level = 5;
        _treeNode.name = _asset.level5;
        treeNode.children.push(_treeNode);
        treeNode = _treeNode;
      }
      treeNode.assets.push(_asset);

    });
  }

  onProjChange(e: Event) {
    var projDoc: AngularFirestoreDocument<Project>;
    var proj: Observable<Project>;
    projDoc = this.afs.doc<Project>('projects/' + (<HTMLSelectElement>e.currentTarget).value);
    proj = projDoc.valueChanges();
    proj.subscribe(proj => {
      this.scService.setCurrProject(proj);
    });
  }

  onMenuClick(e: Event) {
    if(this.enable_menu) {
      this.shwmenudivEl.nativeElement.style.display = "none";
      this.enable_menu = false;
    } else {
      this.shwmenudivEl.nativeElement.style.display = "block";
      this.enable_menu = true;
    }
  }

  logOut(e: Event) {
    this.afAuth.auth.signOut();
  }

  ngOnInit() {
  }

}
