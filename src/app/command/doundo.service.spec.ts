import { TestBed, inject } from '@angular/core/testing';

import { DoundoService } from './doundo.service';

describe('DoundoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DoundoService]
    });
  });

  it('should be created', inject([DoundoService], (service: DoundoService) => {
    expect(service).toBeTruthy();
  }));
});
