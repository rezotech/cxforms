import { Injectable } from '@angular/core';
import { Command } from '../command/command';
import { ScService } from '../sc.service';

@Injectable()
export class DoundoService {
  undoStacks: Array<Array<Command>> = [];
  curr: number;
  first: boolean = false;
  last: boolean = false;

  constructor(private scService : ScService) {
  }

  addCommand(command) {
    var commands : Command[];
    if(this.undoStacks[this.scService.getCxForm().$key]) {
      commands = this.undoStacks[this.scService.getCxForm().$key];
    } else {
      commands = [];
      this.undoStacks[this.scService.getCxForm().$key] = commands;
    }
    commands.push(command);
    this.curr = (commands.length-1);
    this.first = false;
    this.last = false;
    console.log("AddCommand There are " + commands.length + " commands");
  }

  commandSize() {
    var commands : Command[];
    if(this.undoStacks[this.scService.getCxForm().$key]) {
      commands = this.undoStacks[this.scService.getCxForm().$key];
    } else {
      commands = [];
    }
    console.log("There are " + commands.length + " commands");
  }

  redo() {
    var commands : Command[];
    if(this.undoStacks[this.scService.getCxForm().$key]) {
      commands = this.undoStacks[this.scService.getCxForm().$key];
    } else {
      return;
    }
    console.log("do length = " + commands.length);
    console.log("do curr = " + this.curr);
    if(this.last) {
      console.log("beginning of do/undo stack!")
      return;
    }
    commands[this.curr].redo();
    if(this.curr==(commands.length-1)) {
      this.last = true;
    } else {
      this.curr++;
    }
    this.first = false;
  }

  undo() {
    var commands : Command[];
    if(this.undoStacks[this.scService.getCxForm().$key]) {
      commands = this.undoStacks[this.scService.getCxForm().$key];
    } else {
      return;
    }
    if(this.first) {
      console.log("end of do/undo stack!")
      return;
    }
    commands[this.curr].undo();
    if(this.curr==0) {
      this.first = true;
    } else {
      this.curr--;
    }
    this.last = false;
  }
}
