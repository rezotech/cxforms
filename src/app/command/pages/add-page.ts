import { Command } from '../command';
import { Page } from '../../cxforms/shared/page.model';

export class AddPage extends Command {
    constructor() {
        super();
        this.type = "Add Page";
    }
    do() {
        console.log("do()");
        var page = new Page();
        page.formKey = this.scService.getCxForm().$key;
        this.form.no_pages = (this.form.no_pages + 1);
        page.pageno = this.form.no_pages;
        this.key = this.toolbarService.insertPage(page);
        this.toolbarService.updateCxform(this.form);
    }
    undo() {
        this.router.navigate(['/mainwin/' + this.form.$key]);
        this.toolbarService.deletePage(this.key);
        this.scService.getCxForm().no_pages = (this.scService.getCxForm().no_pages - 1);
        this.form.no_pages = (this.form.no_pages - 1);
        this.toolbarService.updateCxform(this.scService.getCxForm());
    }
    redo() {
        var page = new Page();
        this.scService.getCxForm().no_pages = (this.scService.getCxForm().no_pages + 1);
        page.pageno = this.scService.getCxForm().no_pages;
        page.formKey = this.scService.getCxForm().$key;
        this.key = this.toolbarService.insertPage(page);
        this.toolbarService.updateCxform(this.scService.getCxForm());
    }
}