import { ToolbarService } from '../toolbar/toolbar.service';
import { ScService } from '../sc.service';
import { Router } from '@angular/router';
import { Cxform } from '../cxforms/shared/cxform.model';

export class Command {
    form : Cxform;
    type : string;
    key : string;
    toolbarService : ToolbarService;
    scService : ScService;
    router : Router;

    do() {}
    undo() {}
    redo() {}

}
