import { Component, OnInit, Input, ViewChild, ElementRef, Renderer } from '@angular/core';
import { TreeNode } from './treenode.model';

@Component({
  selector: 'app-treenode',
  templateUrl: './treenode.component.html',
  styleUrls: ['./treenode.component.css']
})
export class TreenodeComponent implements OnInit {
  @Input() treeNode: TreeNode;
  @Input() cnt: number = 1;
  @ViewChild("tnode", { read: ElementRef }) tnodel: ElementRef;
  @ViewChild("fnode", { read: ElementRef }) fnodel: ElementRef;
  @ViewChild("slot", { read: ElementRef }) slot: ElementRef;
  @ViewChild("plus", { read: ElementRef }) plus: ElementRef;
  @ViewChild("minus", { read: ElementRef }) minus: ElementRef;
  private showing: boolean = true;

  constructor(private renderer: Renderer) { }

  toggleDisplay(e: Event) {
    if(this.showing) {
      this.renderer.setElementStyle(this.slot.nativeElement, 'display', 'none');
      this.renderer.setElementStyle(this.plus.nativeElement, 'display', 'inline-block');
      this.renderer.setElementStyle(this.minus.nativeElement, 'display', 'none');
      this.showing = false;
    } else {
      this.renderer.setElementStyle(this.slot.nativeElement, 'display', 'inline-block');
      this.renderer.setElementStyle(this.plus.nativeElement, 'display', 'none');
      this.renderer.setElementStyle(this.minus.nativeElement, 'display', 'inline-block');
      this.showing = true;
    }
  }

  ngOnInit() {
    this.tnodel.nativeElement
    this.renderer.setElementStyle(this.tnodel.nativeElement, 'padding-left', this.treeNode.offset + "px");
    this.renderer.setElementStyle(this.fnodel.nativeElement, 'padding-left', (this.treeNode.offset + 10) + "px");

  }

}
