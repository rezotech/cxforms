import { Asset } from "../../cxforms/shared/asset.model";
import { Cxform } from "../../cxforms/shared/cxform.model";

export class TreeNode {
    name: string = "tree_node";
    children: TreeNode[] = [];
    assets: Asset[] = [];
    offset: number;
    level: number;

}