import { Component, OnInit, Input } from '@angular/core';
import { Cxform } from '../../../cxforms/shared/cxform.model';
import { ScService } from '../../../sc.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formnode',
  templateUrl: './formnode.component.html',
  styleUrls: ['./formnode.component.css']
})
export class FormnodeComponent implements OnInit {
  @Input() form: Cxform;
  local_pdfUrl: string = "http://localhost:5000/cxforms-6dcec/us-central1/generatePDF";
  serve_pdfUrl: string = "https://us-central1-cxforms-6dcec.cloudfunctions.net/generatePDF";
  pdfUrl: string = this.local_pdfUrl;

  constructor(private scService: ScService, private router: Router) { }

  onClick(e: Event) {
    this.scService.setCurrMode("form");
    this.scService.setCxform(this.form);
    console.log("no pages = " + this.form.no_pages);
    
    // if(this.scService.getDeviceType()!="unknown") {
    //   this.router.navigate(['/mobilemain']);
    // }

  }

  ngOnInit() {
    console.log("ngOnInit window.location = " + window.location);
    if(window.location.hostname.includes("localhost")) {
      this.pdfUrl = this.local_pdfUrl;
    } else {
      this.pdfUrl = this.serve_pdfUrl;
    }
    
  }

}
