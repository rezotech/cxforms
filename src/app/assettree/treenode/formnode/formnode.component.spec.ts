import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormnodeComponent } from './formnode.component';

describe('FormnodeComponent', () => {
  let component: FormnodeComponent;
  let fixture: ComponentFixture<FormnodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormnodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormnodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
