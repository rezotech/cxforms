import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssettreeComponent } from './assettree.component';

describe('AssettreeComponent', () => {
  let component: AssettreeComponent;
  let fixture: ComponentFixture<AssettreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssettreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssettreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
