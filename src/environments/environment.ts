// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCBsD2iip1a3ECa8Ci_ZoxZ_AmYRQbq_EE",
    authDomain: "cxforms-6dcec.firebaseapp.com",
    databaseURL: "https://cxforms-6dcec.firebaseio.com",
    projectId: "cxforms-6dcec",
    storageBucket: "gs://cxforms-6dcec.appspot.com/",
    messagingSenderId: "219824614931"

    // apiKey: "AIzaSyCifM8cS552ZZsxVwHXeQAXnoKh_DuWN60",
    // authDomain: "sandboxproj-91f93.firebaseapp.com",
    // databaseURL: "https://sandboxproj-91f93.firebaseio.com",
    // projectId: "sandboxproj-91f93",
    // storageBucket: "sandboxproj-91f93.appspot.com",
    // messagingSenderId: "427407271003"
  }
};
